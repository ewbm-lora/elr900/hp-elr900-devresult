/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/*===========================================================================

 	 	 	 	 	 	 INCLUDE FILES FOR MODULE

 ===========================================================================*/
#include "main.h"
#include "gpio.h"
#include "flash.h"
#include "stdbool.h"
#include "usart.h"
#include "rtc.h"




#define APP_OFFSET              0x4000

#define TX_TIMEOUT          	((uint32_t)100)
#define RX_TIMEOUT          	HAL_MAX_DELAY



/**
  * @}
  */

/* Exported constants --------------------------------------------------------*/
/* Packet structure defines */
#define PACKET_HEADER_SIZE      ((uint32_t)3)
#define PACKET_DATA_INDEX       ((uint32_t)4)
#define PACKET_START_INDEX      ((uint32_t)1)
#define PACKET_NUMBER_INDEX     ((uint32_t)2)
#define PACKET_CNUMBER_INDEX    ((uint32_t)3)
#define PACKET_TRAILER_SIZE     ((uint32_t)2)
#define PACKET_OVERHEAD_SIZE    (PACKET_HEADER_SIZE + PACKET_TRAILER_SIZE - 1)
#define PACKET_SIZE             ((uint32_t)128)
#define PACKET_1K_SIZE          ((uint32_t)1024)

/* /-------- Packet in IAP memory ------------------------------------------\
 * | 0      |  1    |  2     |  3   |  4      | ... | n+4     | n+5  | n+6  |
 * |------------------------------------------------------------------------|
 * | unused | start | number | !num | data[0] | ... | data[n] | crc0 | crc1 |
 * \------------------------------------------------------------------------/
 * the first byte is left unused for memory alignment reasons                 */

#define FILE_NAME_LENGTH        ((uint32_t)64)
#define FILE_SIZE_LENGTH        ((uint32_t)16)

#define SOH                     ((uint8_t)0x01)  /* start of 128-byte data packet */
#define STX                     ((uint8_t)0x02)  /* start of 1024-byte data packet */
#define EOT                     ((uint8_t)0x04)  /* end of transmission */
#define ACK                     ((uint8_t)0x06)  /* acknowledge */
#define NAK                     ((uint8_t)0x15)  /* negative acknowledge */
#define CA                      ((uint32_t)0x18) /* two of these in succession aborts transfer */
#define CRC16                   ((uint8_t)0x43)  /* 'C' == 0x43, request 16-bit CRC */
#define NEGATIVE_BYTE           ((uint8_t)0xFF)

#define ABORT1                  ((uint8_t)0x41)  /* 'A' == 0x41, abort by user */
#define ABORT2                  ((uint8_t)0x61)  /* 'a' == 0x61, abort by user */

#define NAK_TIMEOUT             ((uint32_t)0x100000)
#define DOWNLOAD_TIMEOUT        ((uint32_t)1500) /* One second retry delay */
#define MAX_ERRORS              ((uint32_t)5)

#define IS_CAP_LETTER(c)    	(((c) >= 'A') && ((c) <= 'F'))
#define IS_LC_LETTER(c)     	(((c) >= 'a') && ((c) <= 'f'))
#define IS_09(c)            	(((c) >= '0') && ((c) <= '9'))
#define ISVALIDHEX(c)       	(IS_CAP_LETTER(c) || IS_LC_LETTER(c) || IS_09(c))
#define ISVALIDDEC(c)       	IS_09(c)
#define CONVERTDEC(c)       	(c - '0')

#define CONVERTHEX_ALPHA(c) 	(IS_CAP_LETTER(c) ? ((c) - 'A'+10) : ((c) - 'a'+10))
#define CONVERTHEX(c)       	(IS_09(c) ? ((c) - '0') : CONVERTHEX_ALPHA(c))



typedef enum {
  COM_OK       = 0x00,
  COM_ERROR    = 0x01,
  COM_ABORT    = 0x02,
  COM_TIMEOUT  = 0x03,
  COM_DATA     = 0x04,
  COM_LIMIT    = 0x05
} COM_StatusTypeDef;


typedef  void           		(*fJumpFunction)(void);


static uint8_t          		aFileName[FILE_NAME_LENGTH];
static uint8_t					aPacketData[PACKET_1K_SIZE + PACKET_DATA_INDEX + PACKET_TRAILER_SIZE];


extern UART_HandleTypeDef		hlpuart1;



HAL_StatusTypeDef HAL_UART_Receive(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);


uint32_t UpdateCRC16(uint32_t crcIn, uint8_t data)
{
	uint32_t crc = crcIn;
	uint32_t in = data | 0x100;

	do {
		crc <<= 1;
		in <<= 1;

		if (in & 0x100) {
			++crc;
		}

		if (crc & 0x10000) {
			crc ^= 0x1021;
		}

	} while (!(in & 0x10000));

	return crc & 0xffffu;
}



uint32_t BOOT_Get_Crc(uint8_t* srcPtr, uint32_t nSize)
{
   uint32_t crc = 0;
   uint8_t* dataEnd = srcPtr + nSize;

   while (srcPtr < dataEnd) {
       crc = UpdateCRC16(crc, *srcPtr++);
   }

   crc = UpdateCRC16(crc, 0);
   crc = UpdateCRC16(crc, 0);

   return crc & 0xffffu;
}




static HAL_StatusTypeDef ReceivePacket(uint8_t *p_data, uint32_t *p_length, uint32_t timeout)
{
  uint32_t crc;
  uint32_t packet_size = 0;

  HAL_StatusTypeDef status;
  uint8_t char1;

  *p_length = 0;
  status = HAL_UART_Receive(&hlpuart1, &char1, 1, timeout);

  if (status == HAL_OK)
  {
    switch (char1)
    {
      case SOH:
        packet_size = PACKET_SIZE;
        break;
      case STX:
        packet_size = PACKET_1K_SIZE;
        break;
      case EOT:
        break;
      case CA:
        if ((HAL_UART_Receive(&hlpuart1, &char1, 1, timeout) == HAL_OK) && (char1 == CA))
        {
          packet_size = 2;
        }
        else
        {
          status = HAL_ERROR;
        }
        break;


      case ABORT1:
      case ABORT2:
        status = HAL_BUSY;
        break;


      default:
        status = HAL_ERROR;
        break;
    }


    *p_data = char1;

    if (packet_size >= PACKET_SIZE )
    {
      status = HAL_UART_Receive(&hlpuart1, &p_data[PACKET_NUMBER_INDEX], packet_size + PACKET_OVERHEAD_SIZE, timeout);

      /* Simple packet sanity check */
      if (status == HAL_OK )
      {
        if (p_data[PACKET_NUMBER_INDEX] != ((p_data[PACKET_CNUMBER_INDEX]) ^ NEGATIVE_BYTE))
        {
          packet_size = 0;
          status = HAL_ERROR;
        }
        else
        {
          /* Check packet CRC */
          crc = p_data[ packet_size + PACKET_DATA_INDEX ] << 8;
          crc += p_data[ packet_size + PACKET_DATA_INDEX + 1 ];
          if (BOOT_Get_Crc(&p_data[PACKET_DATA_INDEX], packet_size) != crc ) {
        	  packet_size = 0;
        	  status = HAL_ERROR;
          }
        }
      }
      else
      {
        packet_size = 0;
      }
    }
  }
  *p_length = packet_size;
  return status;
}



HAL_StatusTypeDef Serial_PutByte( uint8_t param )
{
  /* May be timeouted... */
  if ( hlpuart1.gState == HAL_UART_STATE_TIMEOUT )
  {
	  hlpuart1.gState = HAL_UART_STATE_READY;
  }
  return HAL_UART_Transmit(&hlpuart1, &param, 1, TX_TIMEOUT);

}



uint32_t Str2Int(uint8_t *p_inputstr, uint32_t *p_intnum)
{
  uint32_t i = 0, res = 0;
  uint32_t val = 0;

  if ((p_inputstr[0] == '0') && ((p_inputstr[1] == 'x') || (p_inputstr[1] == 'X')))
  {
    i = 2;
    while ( ( i < 11 ) && ( p_inputstr[i] != '\0' ) )
    {
      if (ISVALIDHEX(p_inputstr[i]))
      {
        val = (val << 4) + CONVERTHEX(p_inputstr[i]);
      }
      else
      {
        /* Return 0, Invalid input */
        res = 0;
        break;
      }
      i++;
    }

    /* valid result */
    if (p_inputstr[i] == '\0')
    {
      *p_intnum = val;
      res = 1;
    }
  }
  else /* max 10-digit decimal input */
  {
    while ( ( i < 11 ) && ( res != 1 ) )
    {
      if (p_inputstr[i] == '\0')
      {
        *p_intnum = val;
        /* return 1 */
        res = 1;
      }
      else if (((p_inputstr[i] == 'k') || (p_inputstr[i] == 'K')) && (i > 0))
      {
        val = val << 10;
        *p_intnum = val;
        res = 1;
      }
      else if (((p_inputstr[i] == 'm') || (p_inputstr[i] == 'M')) && (i > 0))
      {
        val = val << 20;
        *p_intnum = val;
        res = 1;
      }
      else if (ISVALIDDEC(p_inputstr[i]))
      {
        val = val * 10 + CONVERTDEC(p_inputstr[i]);
      }
      else
      {
        /* return 0, Invalid input */
        res = 0;
        break;
      }
      i++;
    }
  }

  return res;
}


void Serial_PutString(char *p_string)
{
  uint16_t length = 0;

  while (p_string[length] != '\0')
  {
    length++;
  }
  HAL_UART_Transmit(&hlpuart1, (uint8_t*)p_string, length, TX_TIMEOUT);

}



void SetDownLoader(uint32_t AppAddr)
{
	uint32_t        i, flash_res,
					packet_length,
					session_done = 0,
					file_done;

	uint32_t        flashdestination,
					filesize = 0;

	uint8_t         blink = 0,
					*file_ptr,
					nLoopCount = 0;

	uint8_t         file_size[FILE_SIZE_LENGTH],
					packets_received;

	COM_StatusTypeDef result = COM_OK;

	/* Initialize flashdestination variable */
	flashdestination = AppAddr;
	while ((session_done == 0) && (result == COM_OK)) {

		packets_received = 0;
		file_done = 0;

		while ((file_done == 0) && (result == COM_OK)) {

			if(packets_received == 0) {
				nLoopCount++;
				if(nLoopCount >= 30) {
					file_done = 1;
					session_done = 1;
					break;
				}
			}

			switch (ReceivePacket(aPacketData, &packet_length, DOWNLOAD_TIMEOUT)) {

				case HAL_OK:

					HAL_Delay(100);

					switch (packet_length) {
						case 2:
							/* Abort by sender */
							Serial_PutByte(ACK);
							result = COM_ABORT;
							break;


						case 0:
							/* End of transmission */
							Serial_PutByte(ACK);
							file_done = 1;
							break;


						default:
							/* Normal packet */
							if (aPacketData[PACKET_NUMBER_INDEX] != packets_received) {
								Serial_PutByte(NAK);
							}
							else {

								if (packets_received == 0) {

									/* File name packet */
									if (aPacketData[PACKET_DATA_INDEX] != 0) {

										/* File name extraction */
										i = 0;
										file_ptr = aPacketData + PACKET_DATA_INDEX;
										while ( (*file_ptr != 0) && (i < FILE_NAME_LENGTH)) {
											aFileName[i++] = *file_ptr++;
										}

										/* File size extraction */
										aFileName[i++] = '\0';
										i = 0;
										file_ptr ++;

										while ( (*file_ptr != ' ') && (i < FILE_SIZE_LENGTH)) {
											file_size[i++] = *file_ptr++;
										}
										file_size[i++] = '\0';
										Str2Int(file_size, &filesize);

										/* erase user application area */
										flash_res = FLASH_If_Erase(AppAddr, filesize);

										if (flash_res != FLASHIF_OK) {
											Serial_PutByte(CA);
											Serial_PutByte(CA);
										}
										else {
											Serial_PutByte(ACK);
											Serial_PutByte(CRC16);
										}


										flashdestination = AppAddr;

									}

									/* File header packet is empty, end session */
									else {
										Serial_PutByte(ACK);
										file_done = 1;
										session_done = 1;
										break;
									}
								}

								else /* Data packet */ {

									flash_res = FLASH_If_Write(flashdestination, (uint64_t*)&aPacketData[PACKET_DATA_INDEX], packet_length);
									flashdestination += packet_length;


									if (flash_res == FLASHIF_OK) {
										Serial_PutByte(ACK);
									}
									else /* An error occurred while writing to Flash memory */
									{
										/* End session */
										Serial_PutByte(CA);
										Serial_PutByte(CA);
										result = COM_DATA;
									}

								}

								packets_received ++;
								if(packets_received == 0xFF) {
									packets_received = 1;
								}

						}
						break;
					}
					break;


				case HAL_BUSY: /* Abort actually */

					Serial_PutByte(CA);
					Serial_PutByte(CA);
					result = COM_ABORT;
					break;


				default:
					/* BOOT MODE COMMAND */
					Serial_PutByte(CRC16); /* Ask for a packet */
					blink = !blink;
					break;
			}
		}
	}
}




/*============================================================

 FUNCTION main

 DESCRIPTION
 This is a main application

 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
int main(void)
{
    uint32_t		AppAddr;
    uint32_t		EfsAddr;
    uint32_t		StackAddr;
    uint32_t    	JumpAddr;
    uint32_t		ResetReson;
    GPIO_PinState	pinBoot;
    fJumpFunction   setJumpFunction;


    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();


    MX_GPIO_Init();

    AppAddr = (FLASH_BASE + APP_OFFSET);
    EfsAddr = (FLASH_BASE + FLASH_SIZE) - FLASH_PAGE_SIZE;

    StackAddr = (*(__IO uint32_t*)AppAddr);
    ResetReson = (*(__IO uint32_t*)EfsAddr);

    pinBoot = BOOT_PIN_Read();

    if (StackAddr != 0x20008000 || ResetReson == 0xFF05FF04 || pinBoot == GPIO_PIN_SET) {

    	MX_LPUART1_UART_Init();
    	MX_RTC_Init();

		Serial_PutString("BOOT Version 1.0.4\r\n");
		SetDownLoader(AppAddr);
		HAL_Delay(500);
	}

	StackAddr = (*(__IO uint32_t*)AppAddr);
	if(StackAddr == 0x20008000) {
		JumpAddr = *(__IO uint32_t*) (AppAddr + 4);
		setJumpFunction = (fJumpFunction)JumpAddr;

		__set_MSP(StackAddr);

		// HAL_SuspendTick();
		// HAL_StopTick();

		setJumpFunction();
	}







    
}


void Error_Handler(void)
{
	// PRINTFD("Error_Handler \r\n");
}


/*===========================================================================

 FUNCTION SystemClock_Config

 DESCRIPTION
  System Clock Configuration

 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
void SystemClock_Config(void)
{

   RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
   RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

   /** Configure LSE Drive Capability
    */
   __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
   /** Configure the main internal regulator output voltage
    */
   __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
   /** Initializes the CPU, AHB and APB busses clocks
    */

   /* HSE : High speed external 
   ** LSE : Low speed external
   */
   RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
   RCC_OscInitStruct.HSEState = RCC_HSE_ON;
   RCC_OscInitStruct.LSEState = RCC_LSE_ON;
   RCC_OscInitStruct.HSEDiv = RCC_HSE_DIV1;
   RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
   if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
      Error_Handler();
   }
   /** Configure the SYSCLKSource, HCLK, PCLK1 and PCLK2 clocks dividers
    */
   RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK3 | RCC_CLOCKTYPE_HCLK
         | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
   RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
   RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
   RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
   RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
   RCC_ClkInitStruct.AHBCLK3Divider = RCC_SYSCLK_DIV1;

   if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK) {
      Error_Handler();
   }

}



void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
  HAL_IncTick();
}



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

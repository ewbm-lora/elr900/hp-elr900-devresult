/*===========================================================================

 	 	 	 	 	 	 INCLUDE FILES FOR MODULE

 ===========================================================================*/
#include "main.h"
#include "flash.h"
#include "sys_app.h"
#include <string.h>


/*===========================================================================

 DEFINITIONS AND DECLARATIONS FOR MODULE

 This section contains definitions for constants, macros, types, variables
 and other items needed by this module.

 ===========================================================================*/
#define REAL_EFS_SIZE						1024
#define MAX_EFS_MEMORY_SIZE					((REAL_EFS_SIZE/8)*8)

/*===========================================================================

                        LOCAL DEFINITIONS

===========================================================================*/
static uint8_t		            EFS_LOAD_BUFFER[MAX_EFS_MEMORY_SIZE+8];

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/
sEfsItemType *FLASH_EFS_GetMemory(void)
{
	sEfsItemType *pRet = (sEfsItemType*)EFS_LOAD_BUFFER;
	return pRet;
}




/*===========================================================================

 FUNCTION MX_FLASH_Init

 DESCRIPTION
   Initialize internal flash memory.

 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
void MX_FLASH_Init(void)
{
	FLASH_EFS_Read();
}



/*===========================================================================

 FUNCTION FLASH_EFS_Erase

 DESCRIPTION
   Internal flash memory erase.

 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
uint32_t FLASH_If_Erase(uint32_t start, uint32_t size)
{
    FLASH_EraseInitTypeDef desc;
    uint32_t result = FLASHIF_OK;

    HAL_FLASH_Unlock();

    start = (start - FLASH_BASE);
    start = start / FLASH_PAGE_SIZE;
    desc.Page = start;

	size += (FLASH_PAGE_SIZE - 1);
	desc.NbPages = size / FLASH_PAGE_SIZE ;


	desc.TypeErase = FLASH_TYPEERASE_PAGES;


	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR | FLASH_FLAG_CFGBSY);

	if (HAL_FLASHEx_Erase(&desc, &result) != HAL_OK) {
		result = FLASHIF_ERASEKO;
	}
	else {
		result = FLASHIF_OK;
	}


    HAL_FLASH_Lock();

    return result;
}


/*===========================================================================

 FUNCTION FLASH_If_Write

 DESCRIPTION
   Internal flash memory write.
   
 PARAMETERS
 None.

 RETURN VALUE
 None.

 DEPENDENCIES
 None.

 SIDE EFFECTS
 None.
 ===========================================================================*/
uint32_t FLASH_If_Write(uint32_t destination, uint64_t* p_source, uint32_t length)
{
	HAL_StatusTypeDef status;

    uint32_t result = FLASHIF_OK;
    uint64_t *p_actual = p_source;
    uint32_t size = (length + 3) / 8;

    HAL_FLASH_Unlock();

    for(int i=0; i<size; i++) {

    	/* Write the buffer to the memory */
    	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPTVERR | FLASH_FLAG_CFGBSY | FLASH_SR_PROGERR | FLASH_SR_FASTERR);

    	status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, destination, (uint64_t)*p_actual);

        if (status == HAL_OK) {
            /* Increment the memory pointers */
            destination += (8);
            p_actual += 1;
        }
        else {
        	result = FLASHIF_WRITING_ERROR;
        }

        if ( status != HAL_OK ) {
            break;
        }
    }

    HAL_FLASH_Lock();

    return result;
}




uint32_t FLASH_EFS_Read(void)
{
	uint32_t dwPageCount;
	uint32_t destination;

    uint32_t result = FLASHIF_OK;
    uint64_t *p_actual = (uint64_t*)EFS_LOAD_BUFFER;
    uint32_t size = MAX_EFS_MEMORY_SIZE / 8;

    HAL_FLASH_Unlock();

    dwPageCount = (FLASH_SIZE / FLASH_PAGE_SIZE);
    destination = (dwPageCount-1) * FLASH_PAGE_SIZE;

    for(int i=0; i<size; i++) {

    	*p_actual = (*(__IO uint64_t*)destination);

		/* Increment the memory pointers */
		destination += (8);
		p_actual += 1;

    }

    HAL_FLASH_Lock();


    return result;
}

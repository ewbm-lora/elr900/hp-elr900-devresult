
#ifndef __FLASH_H__
#define __FLASH_H__



#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/





typedef enum {
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR,
  FLASHIF_PROTECTION_ERRROR
}sFlashErrType;


/* If the EFS Changed -------------------*/
#define EFS_INIT_DONE       0x58762539



typedef struct {
    uint32_t	dwResetReson;
    uint32_t    dwEfsInit;
    uint32_t    dwEfsSize;
}sEfsItemType;


void			MX_FLASH_Init(void);

sEfsItemType	*FLASH_EFS_GetMemory(void);
void            FLASH_EFS_FactoryReset();
uint32_t		FLASH_If_Write(uint32_t destination, uint64_t* p_source, uint32_t length);
uint32_t		FLASH_EFS_Read(void);

uint32_t        FLASH_If_Erase(uint32_t start, uint32_t size);

#ifdef __cplusplus
}
#endif

#endif /* __FLASH_H__ */



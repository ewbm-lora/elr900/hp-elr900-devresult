/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    lora_command.c
  * @author  MCD Application Team
  * @brief   Main command driver dedicated to command AT
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "platform.h"
#include "lora_at.h"
#include "lora_command.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* comment the following to have help message */
/* #define NO_HELP */
/* #define AT_RADIO_ACCESS */

/* Private typedef -----------------------------------------------------------*/
/**
  * @brief  Structure defining an AT Command
  */
struct ATCommand_s
{
  const char *string;                       /*< command string, after the "AT" */
  const int32_t size_string;                /*< size of the command string, not including the final \0 */
  ATError_t (*get)(const char *param);     /*< =? after the string to get the current value*/
  ATError_t (*set)(const char *param);     /*< = (but not =?\0) after the string to set a value */
  ATError_t (*run)(const char *param);     /*< \0 after the string - run the command */
};

/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
#define CMD_SIZE                        540
#define CIRC_BUFF_SIZE                  8

/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/**
  * @brief  Array corresponding to the description of each possible AT Error
  */
static const char *const ATError_description[] =
{
  "\r\nOK\r\n",                     /* AT_OK */
  "\r\nAT_ERROR\r\n",               /* AT_ERROR */
  "\r\nAT_PARAM_ERROR\r\n",         /* AT_PARAM_ERROR */
  "\r\nAT_BUSY_ERROR\r\n",          /* AT_BUSY_ERROR */
  "\r\nAT_TEST_PARAM_OVERFLOW\r\n", /* AT_TEST_PARAM_OVERFLOW */
  "\r\nAT_NO_NETWORK_JOINED\r\n",   /* AT_NO_NET_JOINED */
  "\r\nAT_RX_ERROR\r\n",            /* AT_RX_ERROR */
  "\r\nAT_NO_CLASS_B_ENABLE\r\n",   /* AT_NO_CLASS_B_ENABLE */
  "\r\nAT_DUTYCYCLE_RESTRICTED\r\n", /* AT_DUTYCYCLE_RESTRICTED */
  "\r\nAT_CRYPTO_ERROR\r\n",        /* AT_CRYPTO_ERROR */
  "\r\nerror unknown\r\n",          /* AT_MAX */
};

/**
  * @brief  Array of all supported AT Commands
  */
static const struct ATCommand_s ATCommand[] =
{
    /* General commands */
    {
        .string = AT_RESET,
        .size_string = sizeof(AT_RESET) - 1,
        .get = AT_return_error,
        .set = AT_return_error,
        .run = AT_reset,
    },

    {
        .string = AT_DBGL,
        .size_string = sizeof(AT_DBGL) - 1,
        .get = AT_verbose_get,
        .set = AT_verbose_set,
        .run = AT_verbose_get,
    },

    {
        .string = AT_VER,
        .size_string = sizeof(AT_VER) - 1,
        .get = AT_version_get,
        .set = AT_return_error,
        .run = AT_version_get,
    },

    {
        .string = AT_REGION,
        .size_string = sizeof(AT_REGION) - 1,
        .get = AT_Region_get,
        .set = AT_Region_set,
        .run = AT_Region_get,
    },

    {
        .string = AT_DEVEUI,
        .size_string = sizeof(AT_DEVEUI) - 1,
        .get = AT_DevEUI_get,
        .set = AT_DevEUI_set,
        .run = AT_DevEUI_get,
    },

    {
        .string = AT_DADDR,
        .size_string = sizeof(AT_DADDR) - 1,
        .get = AT_DevAddr_get,
        .set = AT_DevAddr_set,
        .run = AT_DevAddr_get,
    },

    {
        .string = AT_PNM,
        .size_string = sizeof(AT_PNM) - 1,
        .get = AT_PublicNetwork_get,
        .set = AT_PublicNetwork_set,
        .run = AT_PublicNetwork_get,
    },

    {
        .string = AT_NJM,
        .size_string = sizeof(AT_NJM) - 1,
        .get = AT_NetworkJoinMode_get,
        .set = AT_NetworkJoinMode_set,
        .run = AT_NetworkJoinMode_get,
    },

    {
        .string = AT_FSET,
        .size_string = sizeof(AT_FSET) - 1,
        .get = AT_set_factoryReset,
        .set = AT_set_factoryReset,
        .run = AT_set_factoryReset,
    },

    {
        .string = AT_TRIM,
        .size_string = sizeof(AT_TRIM) - 1,
        .get = AT_Trim_get,
        .set = AT_Trim_set,
        .run = AT_Trim_get,
    },

    {
        .string = AT_MUFR,
        .size_string = sizeof(AT_MUFR) - 1,
        .get = AT_get_mufr,
        .set = AT_set_mufr,
        .run = AT_get_mufr,
    },

    {
        .string = AT_MCFR,
        .size_string = sizeof(AT_MCFR) - 1,
        .get = AT_get_mcfr,
        .set = AT_set_mcfr,
        .run = AT_get_mcfr,
    },

    {
        .string = AT_NJS,
        .size_string = sizeof(AT_NJS) - 1,
        .get = AT_get_joinstatus,
        .set = AT_return_error,
        .run = AT_get_joinstatus,
    },

    {
        .string = AT_RECVB,
        .size_string = sizeof(AT_RECVB) - 1,
        .get = AT_get_receiveb,
        .set = AT_return_error,
        .run = AT_get_receiveb,
    },

    {
        .string = AT_RECV,
        .size_string = sizeof(AT_RECV) - 1,
        .get = AT_get_receive,
        .set = AT_return_error,
        .run = AT_get_receive,
    },

    {
        .string = AT_SNR,
        .size_string = sizeof(AT_SNR) - 1,
        .get = AT_get_snr,
        .set = AT_return_error,
        .run = AT_get_snr,
    },

    {
        .string = AT_RSSI,
        .size_string = sizeof(AT_RSSI) - 1,
        .get = AT_get_rssi,
        .set = AT_return_error,
        .run = AT_get_rssi,
    },

    {
        .string = AT_DATE,
        .size_string = sizeof(AT_DATE) - 1,
        .get = AT_get_date,
        .set = AT_set_date,
        .run = AT_get_date,
    },

    {
        .string = AT_TIME,
        .size_string = sizeof(AT_TIME) - 1,
        .get = AT_get_time,
        .set = AT_set_time,
        .run = AT_get_time,
    },


    {
        .string = AT_FCU,
        .size_string = sizeof(AT_FCU) - 1,
        .get = AT_UplinkCounter_get,
        .set = AT_UplinkCounter_set,
        .run = AT_UplinkCounter_get,
    },
    
      {
        .string = AT_FCD,
        .size_string = sizeof(AT_FCD) - 1,
        .get = AT_DownlinkCounter_get,
        .set = AT_DownlinkCounter_set,
        .run = AT_DownlinkCounter_get,
      },
    

      {
        .string = AT_LCHK,
        .size_string = sizeof(AT_LCHK) - 1,
        .get = AT_return_error,
        .set = AT_return_error,
        .run = AT_link_check_req,
      },

      {
        .string = AT_DTR,
        .size_string = sizeof(AT_DTR) - 1,
        .get = AT_return_error,
        .set = AT_return_error,
        .run = AT_device_time_req,
      },

    {
        .string = AT_APPEUI,
        .size_string = sizeof(AT_APPEUI) - 1,
        .get = AT_AppEUI_get,
        .set = AT_AppEUI_set,
        .run = AT_AppEUI_get,
    },

    {
        .string = AT_APPKEY,
        .size_string = sizeof(AT_APPKEY) - 1,
        .get = AT_AppKey_get,
        .set = AT_AppKey_set,
        .run = AT_AppKey_get,
    },

    {
        .string = AT_NWKSKEY,
        .size_string = sizeof(AT_NWKSKEY) - 1,
        .get = AT_NwkSKey_get,
        .set = AT_NwkSKey_set,
        .run = AT_NwkSKey_get,
    },

    {
        .string = AT_APPSKEY,
        .size_string = sizeof(AT_APPSKEY) - 1,
        .get = AT_AppSKey_get,
        .set = AT_AppSKey_set,
        .run = AT_AppSKey_get,
    },

    {
        .string = AT_ADR,
        .size_string = sizeof(AT_ADR) - 1,
        .get = AT_ADR_get,
        .set = AT_ADR_set,
        .run = AT_ADR_get,
    },

    {
        .string = AT_DR,
        .size_string = sizeof(AT_DR) - 1,
        .get = AT_DataRate_get,
        .set = AT_DataRate_set,
        .run = AT_DataRate_get,
    },

    {
        .string = AT_DCS,
        .size_string = sizeof(AT_DCS) - 1,
        .get = AT_DutyCycle_get,
        .set = AT_DutyCycle_set,
        .run = AT_DutyCycle_get,
    },

    {
        .string = AT_RX2FQ,
        .size_string = sizeof(AT_RX2FQ) - 1,
        .get = AT_Rx2Frequency_get,
        .set = AT_Rx2Frequency_set,
        .run = AT_Rx2Frequency_get,
    },

    {
        .string = AT_RX2DR,
        .size_string = sizeof(AT_RX2DR) - 1,
        .get = AT_Rx2DataRate_get,
        .set = AT_Rx2DataRate_set,
        .run = AT_Rx2DataRate_get,
        },


    {
        .string = AT_RX1DL,
        .size_string = sizeof(AT_RX1DL) - 1,
        .get = AT_Rx1Delay_get,
        .set = AT_Rx1Delay_set,
        .run = AT_Rx1Delay_get,
    },

    {
        .string = AT_RX2DL,
        .size_string = sizeof(AT_RX2DL) - 1,
        .get = AT_Rx2Delay_get,
        .set = AT_Rx2Delay_set,
        .run = AT_Rx2Delay_get,
    },

        
    {
        .string = AT_JN1DL,
        .size_string = sizeof(AT_JN1DL) - 1,
        .get = AT_JoinAcceptDelay1_get,
        .set = AT_JoinAcceptDelay1_set,
        .run = AT_JoinAcceptDelay1_get,
        },
    
    {
        .string = AT_JN2DL,
        .size_string = sizeof(AT_JN2DL) - 1,
        .get = AT_JoinAcceptDelay2_get,
        .set = AT_JoinAcceptDelay2_set,
        .run = AT_JoinAcceptDelay2_get,
    },
        

    {
        .string = AT_NWKID,
        .size_string = sizeof(AT_NWKID) - 1,
        .get = AT_NetworkID_get,
        .set = AT_NetworkID_set,
        .run = AT_NetworkID_get,
    },

    {
        .string = AT_CLASS,
        .size_string = sizeof(AT_CLASS) - 1,
        .get = AT_DeviceClass_get,
        .set = AT_DeviceClass_set,
        .run = AT_DeviceClass_get,
    },


    {
        .string = AT_JOIN,
        .size_string = sizeof(AT_JOIN) - 1,
        .get = AT_Join,
        .set = AT_Join,
        .run = AT_Join,
    },

    {
        .string = AT_AJOIN,
        .size_string = sizeof(AT_AJOIN) - 1,
        .get = AT_get_ajoin,
        .set = AT_set_ajoin,
        .run = AT_get_ajoin,
    },
        
    {
        .string = AT_SENDB,
        .size_string = sizeof(AT_SENDB) - 1,
        .get = AT_return_error,
        .set = AT_SendB,
        .run = AT_return_error,
    },

    {
        .string = AT_SEND,
        .size_string = sizeof(AT_SEND) - 1,
        .get = AT_return_error,
        .set = AT_Send,
        .run = AT_return_error,
    },

    {
        .string = AT_LTIME,
        .size_string = sizeof(AT_LTIME) - 1,
        .get = AT_LocalTime_get,
        .set = AT_return_error,
        .run = AT_LocalTime_get,
    },

    {
        .string = AT_TXP,
        .size_string = sizeof(AT_TXP) - 1,
        .get = AT_TransmitPower_get,
        .set = AT_TransmitPower_set,
        .run = AT_TransmitPower_get,
    },

    {
        .string = AT_LINKC,
        .size_string = sizeof(AT_LINKC) - 1,
        .get = AT_Link_Check,
        .set = AT_Link_Check,
        .run = AT_Link_Check,
    },

    {
        .string = AT_TRSSI,
        .size_string = sizeof(AT_TRSSI) - 1,
        .get = AT_return_error,
        .set = AT_return_error,
        .run = AT_test_rxRssi,
    },

    /* Radio tests commands */
    {
        .string = AT_TTONE,
        .size_string = sizeof(AT_TTONE) - 1,
        .get = AT_return_error,
        .set = AT_test_txTone,
        .run = AT_return_error,
    },

    {
        .string = AT_TCONF,
        .size_string = sizeof(AT_TCONF) - 1,
        .get = AT_test_get_config,
        .set = AT_test_set_config,
        .run = AT_return_error,
    },

    {
        .string = AT_TTX,
        .size_string = sizeof(AT_TTX) - 1,
        .get = AT_return_error,
        .set = AT_test_tx,
        .run = AT_return_error,
    },

    {
        .string = AT_TRX,
        .size_string = sizeof(AT_TRX) - 1,
        .get = AT_return_error,
        .set = AT_test_rx,
        .run = AT_return_error,
    },

    {
        .string = AT_CERTIF,
        .size_string = sizeof(AT_CERTIF) - 1,
        .get = AT_return_error,
        .set = AT_Certif,
        .run = AT_return_error,
    },

    {
        .string = AT_TTH,
        .size_string = sizeof(AT_TTH) - 1,
        .get = AT_return_error,
        .set = AT_test_tx_hopping,
        .run = AT_return_error,
    },


     {
        .string = AT_TXTT,
        .size_string = sizeof(AT_TXTT) - 1,
        .get = AT_return_error,
        .set = AT_test_tx_LBT,
        .run = AT_test_tx_LBT,
    },
    
    {
        .string = AT_RXTT,
        .size_string = sizeof(AT_RXTT) - 1,
        .get = AT_return_error,
        .set = AT_test_rx_LBT,
        .run = AT_test_rx_LBT,
    },

    {
        .string = AT_RXTX,
        .size_string = sizeof(AT_RXTX) - 1,
        .get = AT_return_error,
        .set = AT_test_rxtx_LBT,
        .run = AT_test_rxtx_LBT,
    },

    {
        .string = AT_TOFF,
        .size_string = sizeof(AT_TOFF) - 1,
        .get = AT_return_error,
        .set = AT_return_error,
        .run = AT_test_stop,
    },

    {
        .string = AT_PPARAM,
        .size_string = sizeof(AT_PPARAM) - 1,
        .get = AT_power_parm,
        .set = AT_power_parm,
        .run = AT_power_parm,
    },

    {
        .string = AT_SLEEP,
        .size_string = sizeof(AT_SLEEP) - 1,
        .get = AT_Sleep_set,
        .set = AT_Sleep_set,
        .run = AT_Sleep_set,
    },
    
    {
        .string = AT_AINF,
        .size_string = sizeof(AT_AINF) - 1,
        .get = AT_AINF_set,
        .set = AT_AINF_set,
        .run = AT_AINF_set,
    },
    
    {
        .string = AT_LMSTATE,
        .size_string = sizeof(AT_LMSTATE) - 1,
        .get = AT_LoRaState_get,
        .set = AT_LoRaState_get,
        .run = AT_LoRaState_get,
    },
    {
        .string = AT_BDLOAD,
        .size_string = sizeof(AT_BDLOAD) - 1,
        .get = AT_set_bdload,
        .set = AT_set_bdload,
        .run = AT_set_bdload,
    },
        

#ifdef AT_RADIO_ACCESS
    {
        .string = AT_REGW,
        .size_string = sizeof(AT_REGW) - 1,
        .get = AT_return_error,
        .set = AT_write_register,
        .run = AT_return_error,
    },

    {
        .string = AT_REGR,
        .size_string = sizeof(AT_REGR) - 1,
        .get = AT_return_error,
        .set = AT_read_register,
        .run = AT_return_error,
    },
    
#endif /*AT_RADIO_ACCESS*/


};





static char circBuffer[CIRC_BUFF_SIZE+3];
static char command[CMD_SIZE+3];
static unsigned cmd_index = 0;
static uint32_t widx = 0;
static uint32_t ridx = 0;


/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/

/**
  * @brief  Parse a command and process it
  * @param  cmd The command
  */
static void parse_at_cmd(const char *cmd);

/**
  * @brief  Print a string corresponding to an ATError_t
  * @param  error_type The AT error code
  */
static void com_error(ATError_t error_type);

/**
  * @brief  CMD_GetChar callback from ADV_TRACE
  * @param  rxChar th char received
  * @param  size
  * @param  error
  */
static void CMD_GetChar(uint8_t *rxChar, uint16_t size, uint8_t error);

/**
  * @brief  CNotifies the upper layer that a character has been received
  */
static void (*NotifyCb)(void);

/**
  * @brief  Remove backspace and its preceding character in the Command string
  * @param  cmd string to process
  * @retval 0 when OK, otherwise error
  */


/* Exported functions --------------------------------------------------------*/
void CMD_Init(void (*CmdProcessNotify)(void))
{
    UTIL_ADV_TRACE_StartRxProcess(CMD_GetChar);
    /* register call back*/
    if (CmdProcessNotify != NULL) {
        NotifyCb = CmdProcessNotify;
    }
    widx = 0;
    ridx = 0;
}





void CMD_AT_Process(void)
{
    bool bProcess = true;

    while (bProcess) {
        
        if(ridx == widx) {
            break;
        }

        if(cmd_index>=9 && (memcmp(command, "AT+SENDH=", 9) == 0) ) {  // AT+SENDB=PORT,TYPE,LENG,DATA
            uint8_t MsgLength = 15;
            if( cmd_index >= MsgLength ) {
                MsgLength = 9 + 3 + command[11];
                
                if(cmd_index >= MsgLength) {
                    com_error(AT_SendH((const char *)&command[9]));
                    cmd_index = 0;
                }
                else {
                    cmd_index++;
                }
            }
            else {
                cmd_index++;
            }
            
        }
        else {
            if ((circBuffer[ridx] == '\r') || (circBuffer[ridx] == '\n')) {
                
                if (cmd_index != 0)  {
                    command[cmd_index] = '\0';
                    parse_at_cmd(command);
                    cmd_index = 0;
                }
            }
            else if (cmd_index >= (CMD_SIZE - 1)) {
                cmd_index = 0;
                com_error(AT_TEST_PARAM_OVERFLOW);
            }
            else {
                command[cmd_index++] = circBuffer[ridx];
            }
            ridx = (ridx + 1) % CIRC_BUFF_SIZE;
        }
    }
   
    

}




void CMD_Process(void)
{
    CMD_AT_Process();
    
#if 0    
  if (circBuffOverflow == 1)
  {
    com_error(AT_TEST_PARAM_OVERFLOW);
    /*Full flush in case of overflow */
    UTILS_ENTER_CRITICAL_SECTION();
    ridx = widx;
    charCount = 0;
    circBuffOverflow = 0;
    UTILS_EXIT_CRITICAL_SECTION();
    i = 0;
  }

  while (charCount != 0)
  {
#if 0 /* echo On    */
    AT_PPRINTF("%c", circBuffer[ridx]);
#endif /* 0 */

    if (circBuffer[ridx] == AT_ERROR_RX_CHAR)
    {
      ridx++;
      if (ridx == CIRC_BUFF_SIZE)
      {
        ridx = 0;
      }
      UTILS_ENTER_CRITICAL_SECTION();
      charCount--;
      UTILS_EXIT_CRITICAL_SECTION();
      com_error(AT_RX_ERROR);
      i = 0;
    }
    else if ((circBuffer[ridx] == '\r') || (circBuffer[ridx] == '\n'))
    {
      ridx++;
      if (ridx == CIRC_BUFF_SIZE)
      {
        ridx = 0;
      }
      UTILS_ENTER_CRITICAL_SECTION();
      charCount--;
      UTILS_EXIT_CRITICAL_SECTION();

      if (i != 0)
      {
        command[i] = '\0';
        UTILS_ENTER_CRITICAL_SECTION();
        CMD_ProcessBackSpace(command);
        UTILS_EXIT_CRITICAL_SECTION();
        parse_cmd(command);
        i = 0;
      }
    }
    else if (i == (CMD_SIZE - 1))
    {
      i = 0;
      com_error(AT_TEST_PARAM_OVERFLOW);
    }
    else
    {
      command[i++] = circBuffer[ridx++];
      if (ridx == CIRC_BUFF_SIZE)
      {
        ridx = 0;
      }
      UTILS_ENTER_CRITICAL_SECTION();
      charCount--;
      UTILS_EXIT_CRITICAL_SECTION();
    }
  }

#endif

}




static void CMD_GetChar(uint8_t *rxChar, uint16_t size, uint8_t error)
{
    uint32_t next_widx = (widx + 1) % CIRC_BUFF_SIZE;
    
    if(next_widx != ridx) {
        circBuffer[widx] = *rxChar;
        widx = next_widx;
    }

    if (NotifyCb != NULL) {
        NotifyCb();
    }
}





static void parse_at_cmd(const char *cmd)
{
    int32_t     i;
    ATError_t  status = AT_OK;
    const struct ATCommand_s *Current_ATCommand;
    
    if ((cmd[0] != 'A') || (cmd[1] != 'T')) {
        status = AT_ERROR;
    }
    else if (cmd[2] == '\0') {
        
    }
    else if (cmd[2] == '?')
    {

    }
    else {

        status = AT_ERROR;
        cmd += 2;
    
        for (i = 0; i < (sizeof(ATCommand) / sizeof(struct ATCommand_s)); i++) {

            if (strncmp(cmd, ATCommand[i].string, ATCommand[i].size_string) == 0) {

                Current_ATCommand = &(ATCommand[i]);
                cmd += Current_ATCommand->size_string;

                /* parse after the command */
                switch (cmd[0]) {
                    case '\0':    /* nothing after the command */
                        status = Current_ATCommand->run(cmd);
                        break;

                    case '=':
                        if ((cmd[1] == '?') && (cmd[2] == '\0')) {
                            status = Current_ATCommand->get(cmd + 1);
                        }
                        else {
                            status = Current_ATCommand->set(cmd + 1);
                        }
                        break;
                        
                    case '?':
                        status = AT_OK;
                        break;

                    default:
                        break;
                }

                break;
            }
        }
    }

    com_error(status);

  
}

static void com_error(ATError_t error_type)
{
  /* USER CODE BEGIN com_error_1 */

  /* USER CODE END com_error_1 */
  if (error_type > AT_MAX)
  {
    error_type = AT_MAX;
  }
  AT_PRINTF(ATError_description[error_type]);
  /* USER CODE BEGIN com_error_2 */

  /* USER CODE END com_error_2 */
}

/* USER CODE BEGIN PrFD */

/* USER CODE END PrFD */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/**
  ******************************************************************************
  * @file    lora_app.c
  * @author  MCD Application Team
  * @brief   Application of the LRWAN Middleware
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "Region.h" /* Needed for LORAWAN_DEFAULT_DATA_RATE */
#include "sys_app.h"
#include "lora_app.h"
#include "stm32_seq.h"
#include "stm32_timer.h"
#include "utilities_def.h"
#include "lora_app_version.h"
#include "lorawan_version.h"
#include "subghz_phy_version.h"
#include "lora_info.h"
#include "LmHandler.h"
#include "lora_command.h"
#include "lora_at.h"
#include "flash.h"
#include "main.h"


/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private function prototypes -----------------------------------------------*/
/**
  * @brief  join event callback function
  * @param  params
  * @retval none
  */
static void OnJoinRequest(LmHandlerJoinParams_t *joinParams);

/**
  * @brief  tx event callback function
  * @param  params
  * @retval none
  */
static void OnTxData(LmHandlerTxParams_t *params);

/**
  * @brief callback when LoRa application has received a frame
  * @param appData data received in the last Rx
  * @param params status of last Rx
  */
static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params);

/*!
 * Will be called each time a Radio IRQ is handled by the MAC layer
 *
 */
static void OnMacProcessNotify(void);

/**
  * @brief  call back when LoRaWan Stack needs update
  */
static void CmdProcessNotify(void);


static void OnJoinRandomTimer( void* context );
static void OnMacReayCheck( void* context );






/* Private variables ---------------------------------------------------------*/
/**
  * @brief LoRaWAN handler Callbacks
  */
static LmHandlerCallbacks_t AppLmHandlerCallbacks =
{
  .GetBatteryLevel =           GetBatteryLevel,
  .GetTemperature =            GetTemperatureLevel,
  .GetUniqueId =               GetUniqueId,
  .GetDevAddr =                GetDevAddr,
  .OnMacProcess =              OnMacProcessNotify,
  .OnJoinRequest =             OnJoinRequest,
  .OnTxData =                  OnTxData,
  .OnRxData =                  OnRxData
};

/**
  * @brief LoRaWAN handler parameters
  */
static LmHandlerParams_t AppLmHandlerParams =
{
  .ActiveRegion =             ACTIVE_REGION,
  .DefaultClass =             LORAWAN_DEFAULT_CLASS,
  .AdrEnable =                LORAWAN_ADR_STATE,
  .TxDatarate =               LORAWAN_DEFAULT_DATA_RATE,
  .PingPeriodicity =          LORAWAN_DEFAULT_PING_SLOT_PERIODICITY
};


static TimerEvent_t		MacReadyCheckTimer;
static TimerEvent_t     JoinRandomTimer;
static SysTime_t        timeInitialize;




/* USER CODE END PV */

/* Exported functions ---------------------------------------------------------*/
/* USER CODE BEGIN EF */

/* USER CODE END EF */

void LoRaWAN_Init(void)
{
    uint8_t *pData;

    timeInitialize = SysTimeGetMcuTime();

    CMD_Init(CmdProcessNotify);

    /*Set verbose LEVEL*/
    UTIL_ADV_TRACE_SetVerboseLevel(FLASH_EFS_GetMemory()->uDbgLevel);

    /* USER CODE END LoRaWAN_Init_1 */
    UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_LmHandlerProcess), UTIL_SEQ_RFU, LmHandlerProcess);
    UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_Vcom), UTIL_SEQ_RFU, CMD_Process);

    /* Init Info table used by LmHandler*/
    LoraInfo_Init();

    TimerInit( &JoinRandomTimer, OnJoinRandomTimer );
    TimerInit( &MacReadyCheckTimer, OnMacReayCheck );    


    /* Init the Lora Stack*/
    LmHandlerInit(&AppLmHandlerCallbacks);
    LmHandlerConfigure(&AppLmHandlerParams);

    PRINTF("\r\n");
    PRINTF("F/W VERSION %s\r\n\n", LORA_APP_VERSION); 

    PRINTF("compile date = %s\r\n\n", __DATE__);

    if(FLASH_EFS_GetMemory()->bOtaa) {
    	PRINTF("Over the Air Activation\r\n");
    }
    else {
    	PRINTF("Activation by Personalization\r\n");
    }

    PRINTF("Network : %s\r\n", (FLASH_EFS_GetMemory()->bPublicNetwork == true) ? "Public" : "Private");
    PRINTF("Class : %c\r\n", FLASH_EFS_GetMemory()->bClass + 'A');

    pData = FLASH_EFS_GetMemory()->DevEui;
    PRINTF("DevEui : %02x%02x%02x%02x%02x%02x%02x%02x \r\n",
            pData[0], pData[1], pData[2], pData[3], pData[4], pData[5], pData[6], pData[7]);

    PRINTF("DevAddr : %d\r\n", FLASH_EFS_GetMemory()->dwDevAddr);
    PRINTF("Boot completed\r\n\r\n");

        
    if(FLASH_EFS_GetMemory()->bAutoJoin) {
        AT_Join(NULL);
    }

    
    
}




/* Private functions ---------------------------------------------------------*/
/* USER CODE BEGIN PrFD */

/* USER CODE END PrFD */

static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params)
{
    uint8_t     Port = 0;
    uint8_t     BufferSize = 0;
    uint8_t     ReceivedDataSize = 0;
    uint8_t     *Buffer = NULL;

    uint32_t    DownlinkCounter = 0;
    int8_t      Rssi = 0;
    int8_t      Snr = 0;

    if (appData != NULL) {
        Port = appData->Port;
        BufferSize = appData->BufferSize;
        Buffer = appData->Buffer;
        if(Buffer == NULL) {
            BufferSize = 0;
        }

        if (242 <= appData->BufferSize) {
            ReceivedDataSize = 242;
        }
        else {
            ReceivedDataSize = appData->BufferSize;
        }

    }

    if (params != NULL) {
        DownlinkCounter = params->DownlinkCounter;
        Rssi = params->Rssi;
        Snr = params->Snr;
    }


    if(ReceivedDataSize > 0 &&  Buffer != NULL) {

    	PRINTF("SEND : RX,%d,%d,%d,%d,%d,%d\r\n", params->Status, Port, DownlinkCounter, ReceivedDataSize, Rssi, Snr);
		PRINTF("RX_MSG :");

		for (uint8_t i = 0; i < BufferSize; i++) {
		  PRINTF("%02x", Buffer[i]);
		}
		PRINTF("\r\n");

    }

    // TimerSetValue(&MacReadyCheckTimer, 250);
    // TimerStart(&MacReadyCheckTimer);
    
    AT_set_receive(Port, BufferSize, Buffer, Snr, Rssi);

    

}

/* USER CODE END PrFD_LedEvents */

static void OnTxData(LmHandlerTxParams_t *params)
{
    TimerStop( &MacReadyCheckTimer );
    if( LmHandlerJoinStatus() != LORAMAC_HANDLER_SET ) {
        return;
    }
    
    if(params == NULL || params->IsMcpsConfirm == 0) {
        // PRINTF("SEND : CFM,0,0,0,0,0\r\n");
        return;
    }

   
    PRINTF("SEND : CFM,%d,%d,%d\r\n", params->Status, params->MsgType, params->UplinkCounter);

    TimerSetValue(&MacReadyCheckTimer, 250);
    TimerStart(&MacReadyCheckTimer);
}


bool IsReJoinTimerStarted(void)
{
    return (LmHandlerReJoinCounter() > 0);
}


static void OnJoinRequest(LmHandlerJoinParams_t *joinParams)
{
    int         ranTime;
            
    if (joinParams != NULL) {
        if (joinParams->Status == LORAMAC_HANDLER_SUCCESS) {
            PRINTF("JOINED\r\n\r\n");
            PRINTF("READY\r\n");
            return;
        }
        else {
            ranTime = randr(1, 10);
            ranTime = LmHandlerReJoinGetTime(ranTime, timeInitialize);

            if(LmHandlerReJoinSetReady()) {
            	TimerSetValue( &JoinRandomTimer, ranTime*1000);
				PRINTFD("ReJoin Random Time %d second \r\n", ranTime);
				TimerStart( &JoinRandomTimer);
            }
            else {
            	PRINTF("READY\r\n");
            }
        }

    }
}

static void CmdProcessNotify(void)
{
    UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_Vcom), 0);
}

static void OnMacProcessNotify(void)
{
    UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LmHandlerProcess), CFG_SEQ_Prio_0);
}



static void OnJoinRandomTimer(void* context)
{
    LmHandlerReJoin();
}


static void OnMacReayCheck( void* context )
{
    if(LoRaMacIsBusy() == false) {
        TimerStop( &MacReadyCheckTimer );
        PRINTF("READY\r\n");
     }
     else {
        TimerSetValue( &MacReadyCheckTimer, 100);
        TimerStart( &MacReadyCheckTimer);
     }
}



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    lora_at.c
  * @author  MCD Application Team
  * @brief   AT command API
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "lora_at.h"
#include "sys_app.h"
#include "stm32_tiny_sscanf.h"
#include "lora_app_version.h"
#include "lorawan_version.h"
#include "subghz_phy_version.h"
#include "test_rf.h"
#include "adc_if.h"
#include "stm32_seq.h"
#include "utilities_def.h"
#include "radio.h"
#include "lora_info.h"
#include "flash.h"
#include "secure-element.h"
#include "LmHandler.h"


/* External variables ---------------------------------------------------------*/
extern void SleepStartTimer(void);
extern void SetAlarmTime(int nValue);
extern int GetAlarmTime(void);
extern int GetLoRaMacState(void);
extern bool IsReJoinTimerStarted(void);



/* Private define ------------------------------------------------------------*/
/*!
 * User application data buffer size
 */
#define LORAWAN_APP_DATA_BUFFER_MAX_SIZE            242




/*!
 * User application buffer
 */
static uint8_t AppDataBuffer[LORAWAN_APP_DATA_BUFFER_MAX_SIZE];

/*!
 * User application data structure
 */
static LmHandlerAppData_t AppData = { 0, 0, AppDataBuffer };

static uint8_t              uLastRxBuff[LORAWAN_APP_DATA_BUFFER_MAX_SIZE];
static LmHandlerAppData_t   appLastRxData = {0, 0, uLastRxBuff};
static int8_t               nLastRxSnr = 0;
static int8_t               nLastRxRssi = 0;




/* Dummy data sent periodically to let the tester respond with start test command*/
static UTIL_TIMER_Object_t TxCertifTimer;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/**
  * @brief  Get 4 bytes values in hex
  * @param  from string containing the 16 bytes, something like ab:cd:01:21
  * @param  value buffer that will contain the bytes read
  * @retval The number of bytes read
  */
// static int32_t sscanf_uint32_as_hhx(const char *from, uint32_t *value);

/**
  * @brief  Get 16 bytes values in hex
  * @param  from string containing the 16 bytes, something like ab:cd:01:...
  * @param  pt buffer that will contain the bytes read
  * @retval The number of bytes read
  */
// static int sscanf_16_hhx(const char *from, uint8_t *pt);


/**
  * @brief  Print 16 bytes as %02X
  * @param  pt pointer containing the 16 bytes to print
  */
static void print_16_02x(uint8_t *pt);

/**
  * @brief  Print 8 bytes as %02X
  * @param  pt pointer containing the 8 bytes to print
  */
static void print_8_02x(uint8_t *pt);

/**
  * @brief  Print an int
  * @param  value to print
  */
static void print_d(int32_t value);

/**
  * @brief  Print an unsigned int
  * @param  value to print
  */
static void print_u(uint32_t value);

/**
  * @brief  Certif Rejoin timer callback function
  * @param  context ptr of Certif Rejoin context
  */
static void OnCertifTimer(void *context);

/**
  * @brief  Certif send function
  */
static void CertifSend(void);

/**
  * @brief  Check if character in parameter is alphanumeric
  * @param  Char for the alphanumeric check
  */
static int32_t isHex(char Char);

/**
  * @brief  Converts hex string to a nibble ( 0x0X )
  * @param  Char hex string
  * @retval the nibble. Returns 0xF0 in case input was not an hex string (a..f; A..F or 0..9)
  */
static uint8_t Char2Nibble(char Char);

/**
  * @brief  Convert a string into a buffer of data
  * @param  str string to convert
  * @param  data output buffer
  * @param  Size of input string
  */
static int32_t stringToData(const char *str, uint8_t *data, uint32_t Size);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Exported functions --------------------------------------------------------*/
ATError_t AT_return_ok(const char *param)
{
  return AT_OK;
}

ATError_t AT_return_error(const char *param)
{
  return AT_ERROR;
}

/* --------------- Application events --------------- */
void AT_event_join(LmHandlerJoinParams_t *params)
{
  /* USER CODE BEGIN AT_event_join_1 */

  /* USER CODE END AT_event_join_1 */
  if ((params != NULL) && (params->Status == LORAMAC_HANDLER_SUCCESS))
  {
    AT_PRINTF("+EVT:JOINED\r\n");
  }
  else
  {
    AT_PRINTF("+EVT:JOIN FAILED\r\n");
  }
  /* USER CODE BEGIN AT_event_join_2 */

  /* USER CODE END AT_event_join_2 */
}


void AT_event_confirm(LmHandlerTxParams_t *params)
{
  /* USER CODE BEGIN AT_event_confirm_1 */

  /* USER CODE END AT_event_confirm_1 */
  if ((params != NULL) && (params->MsgType == LORAMAC_HANDLER_CONFIRMED_MSG) && (params->AckReceived != 0))
  {
    AT_PRINTF("+EVT:SEND_CONFIRMED\r\n");
  }
  /* USER CODE BEGIN AT_event_confirm_2 */

  /* USER CODE END AT_event_confirm_2 */
}

/* --------------- General commands --------------- */

ATError_t AT_reset(const char *param)
{
  NVIC_SystemReset();
}



ATError_t AT_verbose_get(const char *param)
{

  AT_PRINTF("DBGL:"); print_u(UTIL_ADV_TRACE_GetVerboseLevel());
  return AT_OK;
}


ATError_t AT_verbose_set(const char *param)
{
  const char *buf = param;
  int32_t lvl_nb;

  /* read and set the verbose level */
  if (1 != tiny_sscanf(buf, "%u", &lvl_nb))
  {
    return AT_PARAM_ERROR;
  }

  if ((lvl_nb > VLEVEL_H) || (lvl_nb < VLEVEL_L))  {
    return AT_PARAM_ERROR;
  }

  UTIL_ADV_TRACE_SetVerboseLevel(lvl_nb);
  FLASH_EFS_GetMemory()->uDbgLevel = lvl_nb;
  FLASH_EFS_Write();

  AT_PRINTF("DBGL:"); print_u(lvl_nb);

  return AT_OK;
  /* USER CODE END AT_verbose_set_2 */
}



ATError_t AT_LocalTime_get(const char *param)
{
  /* USER CODE BEGIN AT_LocalTime_get_1 */

  /* USER CODE END AT_LocalTime_get_1 */
  struct tm localtime;
  SysTime_t UnixEpoch = SysTimeGet();
  UnixEpoch.Seconds -= 18; /*removing leap seconds*/

  UnixEpoch.Seconds += 3600 * 2; /*adding 2 hours*/

  SysTimeLocalTime(UnixEpoch.Seconds,  & localtime);

  AT_PRINTF("LTIME:%02dh%02dm%02ds on %02d/%02d/%04d\r\n",
            localtime.tm_hour, localtime.tm_min, localtime.tm_sec,
            localtime.tm_mday, localtime.tm_mon + 1, localtime.tm_year + 1900);

  return AT_OK;
  /* USER CODE BEGIN AT_LocalTime_get_2 */

  /* USER CODE END AT_LocalTime_get_2 */
}




ATError_t AT_Region_get(const char *param)
{
  LoRaMacRegion_t region;
  if (LmHandlerGetActiveRegion(&region) != LORAMAC_HANDLER_SUCCESS) {
    return AT_PARAM_ERROR;
  }

  if (region > LORAMAC_REGION_RU864) {
    return AT_PARAM_ERROR;
  }


  AT_PRINTF("REGION : %d\r\n", region);
  
  return AT_OK;
}



ATError_t AT_Region_set(const char *param)
{
#if 0
  /* USER CODE BEGIN AT_Region_set_1 */

  /* USER CODE END AT_Region_set_1 */
  LoRaMacRegion_t region;
  if (tiny_sscanf(param, "%hhu", &region) != 1)
  {
    return AT_PARAM_ERROR;
  }
  if (region > LORAMAC_REGION_RU864)
  {
    return AT_PARAM_ERROR;
  }

  if (LmHandlerSetActiveRegion(region) != LORAMAC_HANDLER_SUCCESS)
  {
    return AT_PARAM_ERROR;
  }

  return AT_OK;
#else
  return AT_PARAM_ERROR;
#endif
  
}



ATError_t AT_DevEUI_get(const char *param)
{
    uint8_t devEUI[8];
    if (LmHandlerGetDevEUI(devEUI) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("DEVEUI : "); print_8_02x(devEUI);
    
    return AT_OK;
}



ATError_t AT_DevEUI_set(const char *param)
{
    uint8_t devEui[8];
    if (tiny_sscanf(param, "%02x%02x%02x%02x%02x%02x%02x%02x",
                    &devEui[0], &devEui[1], &devEui[2], &devEui[3],
                    &devEui[4], &devEui[5], &devEui[6], &devEui[7]) != 8)
    {
        return AT_PARAM_ERROR;
    }
    
    if (LORAMAC_HANDLER_SUCCESS != LmHandlerSetDevEUI(devEui)) {
        return AT_ERROR;
    }
    
    memcpy(FLASH_EFS_GetMemory()->DevEui, devEui, 8);
    FLASH_EFS_Write();
    
    AT_PRINTF("DEVEUI : "); print_8_02x(devEui);
    
    return AT_OK;

}



ATError_t AT_AppEUI_get(const char *param)
{
    uint8_t appEUI[8];
    if (LmHandlerGetAppEUI(appEUI) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("APPEUI : "); print_8_02x(appEUI);

    return AT_OK;
}



ATError_t AT_AppEUI_set(const char *param)
{
    uint8_t     AppEUI[8];
    
    if (tiny_sscanf(param, "%02x%02x%02x%02x%02x%02x%02x%02x",
                  &AppEUI[0], &AppEUI[1], &AppEUI[2], &AppEUI[3],
                  &AppEUI[4], &AppEUI[5], &AppEUI[6], &AppEUI[7]) != 8)
    {
        return AT_PARAM_ERROR;
    }

    if (LORAMAC_HANDLER_SUCCESS != LmHandlerSetAppEUI(AppEUI)) {
        return AT_ERROR;
    }

    memcpy(FLASH_EFS_GetMemory()->AppEui, AppEUI, 8);
    FLASH_EFS_Write();


    AT_PRINTF("APPEUI : "); print_8_02x(AppEUI);

    return AT_OK;
    
}




ATError_t AT_AppKey_get(const char *param)
{
    uint8_t *AppKey;
    AppKey = SecureElementGetAppKey();
    AT_PRINTF("APPKEY : "); print_16_02x(AppKey);
    return AT_OK;
}

ATError_t AT_AppKey_set(const char *param)
{
    uint8_t AppKey[16];
    MibRequestConfirm_t mibReq;

    if (tiny_sscanf(param, "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                        &AppKey[0], &AppKey[1], &AppKey[2], &AppKey[3],
                        &AppKey[4], &AppKey[5], &AppKey[6], &AppKey[7],
                        &AppKey[8], &AppKey[9], &AppKey[10], &AppKey[11],
                        &AppKey[12], &AppKey[13], &AppKey[14], &AppKey[15]) != 16)
    {
        return AT_PARAM_ERROR;
    }


    mibReq.Type = MIB_APP_KEY;
    mibReq.Param.AppKey = AppKey;
    if (LoRaMacMibSetRequestConfirm(&mibReq) != LORAMAC_STATUS_OK)
    {
        return LORAMAC_HANDLER_ERROR;
    }

    memcpy(FLASH_EFS_GetMemory()->AppKey, AppKey, 16);
    FLASH_EFS_Write();


    
    AT_PRINTF("APPKEY : "); print_16_02x(AppKey);
    return AT_OK;
}




ATError_t AT_NwkSKey_get(const char *param)
{
    uint8_t *NwkSKey;
    NwkSKey = SecureElementGetNwkSKey();

    AT_PRINTF("NWKSKEY : "); print_16_02x(NwkSKey);
    
    return AT_OK;
}


ATError_t AT_NwkSKey_set(const char *param)
{
    uint8_t NwkSKey[16];
    MibRequestConfirm_t mibReq;

    if (tiny_sscanf(param, "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                        &NwkSKey[0], &NwkSKey[1], &NwkSKey[2], &NwkSKey[3],
                        &NwkSKey[4], &NwkSKey[5], &NwkSKey[6], &NwkSKey[7],
                        &NwkSKey[8], &NwkSKey[9], &NwkSKey[10], &NwkSKey[11],
                        &NwkSKey[12], &NwkSKey[13], &NwkSKey[14], &NwkSKey[15]) != 16)
    {
        return AT_PARAM_ERROR;
    }


    mibReq.Type = MIB_NWK_S_KEY;
    mibReq.Param.NwkSKey = NwkSKey;
    if (LoRaMacMibSetRequestConfirm(&mibReq) != LORAMAC_STATUS_OK)
    {
        return LORAMAC_HANDLER_ERROR;
    }

    memcpy(FLASH_EFS_GetMemory()->NwkSKey, NwkSKey, 16);
    FLASH_EFS_Write();


    AT_PRINTF("NWKSKEY : "); print_16_02x(NwkSKey);

    return AT_OK;
}




ATError_t AT_AppSKey_get(const char *param)
{
    uint8_t *AppSKey;
    AppSKey = SecureElementGetAppSKey();

    AT_PRINTF("APPSKEY : "); print_16_02x(AppSKey);
    
    return AT_OK;
}


ATError_t AT_AppSKey_set(const char *param)
{
    uint8_t AppSKey[16];
    MibRequestConfirm_t mibReq;

    if (tiny_sscanf(param, "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                        &AppSKey[0], &AppSKey[1], &AppSKey[2], &AppSKey[3],
                        &AppSKey[4], &AppSKey[5], &AppSKey[6], &AppSKey[7],
                        &AppSKey[8], &AppSKey[9], &AppSKey[10], &AppSKey[11],
                        &AppSKey[12], &AppSKey[13], &AppSKey[14], &AppSKey[15]) != 16)
    {
        return AT_PARAM_ERROR;
    }


    mibReq.Type = MIB_APP_S_KEY;
    mibReq.Param.AppSKey = AppSKey;
    if (LoRaMacMibSetRequestConfirm(&mibReq) != LORAMAC_STATUS_OK)
    {
        return LORAMAC_HANDLER_ERROR;
    }

	memcpy(FLASH_EFS_GetMemory()->AppSKey, AppSKey, 16);
    FLASH_EFS_Write();


    AT_PRINTF("APPSKEY : "); print_16_02x(AppSKey);

    return AT_OK;
}



ATError_t AT_DevAddr_get(const char *param)
{
    uint32_t devAddr;
    if (LmHandlerGetDevAddr(&devAddr) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("DEVADDR : %d \r\n", devAddr);

    return AT_OK;
}

ATError_t AT_DevAddr_set(const char *param)
{
    uint32_t devAddr;
    
    if (tiny_sscanf(param, "%lu",&devAddr) != 1) {
        return AT_PARAM_ERROR;
    }

    if (LORAMAC_HANDLER_SUCCESS != LmHandlerSetDevAddr(devAddr)) {
        return AT_ERROR;
    }

    FLASH_EFS_GetMemory()->dwDevAddr = devAddr;
    FLASH_EFS_Write();

    AT_PRINTF("DEVADDR : %d \r\n", devAddr);

    return AT_OK;
}




ATError_t AT_NetworkJoinMode_get(const char *param)
{
    AT_PRINTF("NJM : %d\r\n", FLASH_EFS_GetMemory()->bOtaa); 
    return AT_OK;
}


ATError_t AT_NetworkJoinMode_set(const char *param)
{
    uint8_t  mode = 1;

    switch (param[0])
    {
        case '0':
            mode = 0;
            break;

        case '1':
            mode = 1;
            break;

        default:
            return AT_PARAM_ERROR;
    }

    FLASH_EFS_GetMemory()->bOtaa = mode;
    FLASH_EFS_Write();

    AT_PRINTF("NJM : %d\r\n", mode); 

    return AT_OK;

}



ATError_t AT_Trim_get(const char *param)
{
    AT_PRINTF("TRIM : %d, %d\r\n", FLASH_EFS_GetMemory()->uTrimValA, FLASH_EFS_GetMemory()->uTrimValB); 
    return AT_OK;
}


ATError_t AT_Trim_set(const char *param)
{
    uint8_t uTrimValA, uTrimValB;
    
    if (tiny_sscanf(param, "%hhu,%hhu", &uTrimValA, &uTrimValB) != 2) {
        return AT_PARAM_ERROR;
    }
    
    FLASH_EFS_GetMemory()->uTrimValA = uTrimValA;
    FLASH_EFS_GetMemory()->uTrimValB = uTrimValB;
    FLASH_EFS_Write();
    
    AT_PRINTF("TRIM : %d %d\r\n", FLASH_EFS_GetMemory()->uTrimValA, FLASH_EFS_GetMemory()->uTrimValB); 
    return AT_OK;
}




ATError_t AT_get_mufr(const char *param)
{
    MibRequestConfirm_t mib;
    mib.Type = MIB_CHANNELS_NB_TRANS;
    LoRaMacMibGetRequestConfirm(&mib);

    AT_PRINTF("MUFR : %d\r\n", mib.Param.ChannelsNbTrans);
  
    return AT_OK;
}



ATError_t AT_set_mufr(const char *param)
{
    uint8_t ChannelNbRep=0;
    MibRequestConfirm_t mibReq;

    if (tiny_sscanf(param,"%d", &ChannelNbRep)!= 1) {
        return AT_PARAM_ERROR;
    }

    if( (ChannelNbRep > 15) || (ChannelNbRep < 1)) {
        return AT_PARAM_ERROR;
    }


    mibReq.Type = MIB_CHANNELS_NB_TRANS;
    mibReq.Param.ChannelsNbTrans = ChannelNbRep;
    if (LoRaMacMibSetRequestConfirm(&mibReq) != LORAMAC_STATUS_OK) {
        return AT_PARAM_ERROR;
    }

    FLASH_EFS_GetMemory()->bChannelNbRep = ChannelNbRep;
    FLASH_EFS_Write();

    AT_PRINTF("MUFR : %d\r\n", ChannelNbRep);

    return AT_OK;
}




ATError_t AT_get_mcfr(const char *param)
{
    AT_PRINTF("MCFR : %d\r\n", FLASH_EFS_GetMemory()->bNbTrials);
    return AT_OK;
}



ATError_t AT_set_mcfr(const char *param)
{
    uint8_t NbTrials=0;

    if (tiny_sscanf(param,"%d", &NbTrials)!= 1) {
        return AT_PARAM_ERROR;
    }

    if( (NbTrials > 8) || (NbTrials < 1)) {
        return AT_PARAM_ERROR;
    }

    FLASH_EFS_GetMemory()->bNbTrials = NbTrials;
    FLASH_EFS_Write();

    AT_PRINTF("MCFR : %d\r\n", NbTrials);

    return AT_OK;
}



ATError_t AT_get_joinstatus(const char *param)
{
    if(LmHandlerJoinStatus() == LORAMAC_HANDLER_SET) {
        AT_PRINTF("NJS : 1\r\n");
    }
    else {
        AT_PRINTF("NJS : 0\r\n");
    }
      

    return AT_OK;
}



ATError_t AT_ADR_get(const char *param)
{
    bool adrEnable;
    if (LmHandlerGetAdrEnable(&adrEnable) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("ADR : "); print_d(adrEnable);


    return AT_OK;
}

ATError_t AT_ADR_set(const char *param)
{
    switch (param[0]) {
        case '0':
        case '1':
            LmHandlerSetAdrEnable(param[0] - '0');
            break;
        
        default:
            return AT_PARAM_ERROR;
    }

    AT_PRINTF("ADR : "); print_d(param[0] - '0');
    return AT_OK;
}



ATError_t AT_TransmitPower_get(const char *param)
{
    int8_t txPower;
    if (LmHandlerGetTxPower(&txPower) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("TXP : "); print_d(txPower);
    return AT_OK;
}


ATError_t AT_TransmitPower_set(const char *param)
{
    int8_t txPower;
    if (tiny_sscanf(param, "%hhu", &txPower) != 1) {
        return AT_PARAM_ERROR;
    }

    if (LmHandlerSetTxPower(txPower) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("TXP : "); print_d(txPower);

    return AT_OK;
}




ATError_t AT_DataRate_get(const char *param)
{
  /* USER CODE BEGIN AT_DataRate_get_1 */

  /* USER CODE END AT_DataRate_get_1 */
  int8_t txDatarate;
  if (LmHandlerGetTxDatarate(&txDatarate) != LORAMAC_HANDLER_SUCCESS)
  {
    return AT_PARAM_ERROR;
  }

  AT_PRINTF("DR : ");  print_d(txDatarate);
  return AT_OK;
  /* USER CODE BEGIN AT_DataRate_get_2 */

  /* USER CODE END AT_DataRate_get_2 */
}


ATError_t AT_DataRate_set(const char *param)
{
  /* USER CODE BEGIN AT_DataRate_set_1 */

  /* USER CODE END AT_DataRate_set_1 */
  int8_t datarate;

  if (tiny_sscanf(param, "%hhu", &datarate) != 1)
  {
    return AT_PARAM_ERROR;
  }
  if ((datarate < 0) || (datarate > 15))
  {
    return AT_PARAM_ERROR;
  }

  if (LmHandlerSetTxDatarate(datarate) != LORAMAC_HANDLER_SUCCESS)
  {
    return AT_ERROR;
  }

  AT_PRINTF("DR : ");  print_d(datarate);
  return AT_OK;

}



ATError_t AT_DutyCycle_get(const char *param)
{

  bool dutyCycleEnable;
  if (LmHandlerGetDutyCycleEnable(&dutyCycleEnable) != LORAMAC_HANDLER_SUCCESS)
  {
    return AT_PARAM_ERROR;
  }

  AT_PRINTF("DCS : ");  print_d(dutyCycleEnable);
  return AT_OK;

}

ATError_t AT_DutyCycle_set(const char *param)
{

  switch (param[0])
  {
    case '0':
      AT_PRINTF("DCS : 0\r\n");
      LmHandlerSetDutyCycleEnable(0);
      break;
      
    case '1':
      AT_PRINTF("DCS : 1\r\n");
      LmHandlerSetDutyCycleEnable(1);
      break;
      
    default:
      return AT_PARAM_ERROR;
  }

  return AT_OK;
}




ATError_t AT_Rx2Frequency_get(const char *param)
{
    RxChannelParams_t rx2Params;
    LmHandlerGetRX2Params(&rx2Params);

    AT_PRINTF("RX2FQ : "); print_d(rx2Params.Frequency);
    return AT_OK;
}


ATError_t AT_Rx2Frequency_set(const char *param)
{
    RxChannelParams_t rx2Params;

    /* Get the current configuration of RX2 */
    LmHandlerGetRX2Params(&rx2Params);

    /* Update the frequency with scanf */
    if (tiny_sscanf(param, "%lu", &(rx2Params.Frequency)) != 1)
    {
        return AT_PARAM_ERROR;
    }
    else if (LmHandlerSetRX2Params(&rx2Params) != LORAMAC_HANDLER_SUCCESS)
    {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("RX2FQ : "); print_d(rx2Params.Frequency);
    return AT_OK;
  
}



ATError_t AT_Rx2DataRate_get(const char *param)
{
    RxChannelParams_t rx2Params;
    LmHandlerGetRX2Params(&rx2Params);

    AT_PRINTF("RX2DR : ");  print_d(rx2Params.Datarate);
    return AT_OK;
}



ATError_t AT_Rx2DataRate_set(const char *param)
{
  RxChannelParams_t rx2Params;

  /* Get the current configuration of RX2 */
  LmHandlerGetRX2Params(&rx2Params);

  /* Update the Datarate with scanf */
  if (tiny_sscanf(param, "%hhu", &(rx2Params.Datarate)) != 1)
  {
    return AT_PARAM_ERROR;
  }
  else if (rx2Params.Datarate > 15)
  {
    return AT_PARAM_ERROR;
  }
  else if (LmHandlerSetRX2Params(&rx2Params) != LORAMAC_HANDLER_SUCCESS)
  {
    return AT_PARAM_ERROR;
  }


  AT_PRINTF("RX2DR : ");  print_d(rx2Params.Datarate);
  return AT_OK;
  
}



ATError_t AT_Rx1Delay_get(const char *param)
{
    uint32_t rxDelay;
    if (LmHandlerGetRx1Delay(&rxDelay) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("RX1DL : "); print_u(rxDelay);
    return AT_OK;
}



ATError_t AT_Rx1Delay_set(const char *param)
{
    uint32_t rxDelay;
    if (tiny_sscanf(param, "%lu", &rxDelay) != 1)
    {
        return AT_PARAM_ERROR;
    }
    else if (LmHandlerSetRx1Delay(rxDelay) != LORAMAC_HANDLER_SUCCESS)
    {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("RX1DL : "); print_u(rxDelay);
    return AT_OK;
  
}

ATError_t AT_Rx2Delay_get(const char *param)
{
    uint32_t rxDelay;
    if (LmHandlerGetRx2Delay(&rxDelay) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("RX2DL : "); print_u(rxDelay);
    return AT_OK;
}

ATError_t AT_Rx2Delay_set(const char *param)
{
    uint32_t rxDelay;
    if (tiny_sscanf(param, "%lu", &rxDelay) != 1) {
        return AT_PARAM_ERROR;
    }
    else if (LmHandlerSetRx2Delay(rxDelay) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("RX2DL : "); print_u(rxDelay);
    return AT_OK;
    
}



ATError_t AT_JoinAcceptDelay1_get(const char *param)
{
    uint32_t rxDelay;
    if (LmHandlerGetJoinRx1Delay(&rxDelay) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("JN1DL : "); print_u(rxDelay);
    return AT_OK;
}



ATError_t AT_JoinAcceptDelay1_set(const char *param)
{
    uint32_t rxDelay;
    if (tiny_sscanf(param, "%lu", &rxDelay) != 1) {
        return AT_PARAM_ERROR;
    }
    else if (LmHandlerSetJoinRx1Delay(rxDelay) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("JN1DL : "); print_u(rxDelay);
    return AT_OK;
    
}


ATError_t AT_JoinAcceptDelay2_get(const char *param)
{
  /* USER CODE BEGIN AT_JoinAcceptDelay2_get_1 */

  /* USER CODE END AT_JoinAcceptDelay2_get_1 */
  uint32_t rxDelay;
  if (LmHandlerGetJoinRx2Delay(&rxDelay) != LORAMAC_HANDLER_SUCCESS)
  {
    return AT_PARAM_ERROR;
  }

  AT_PRINTF("JN2DL : ");print_u(rxDelay);
  return AT_OK;
}


ATError_t AT_JoinAcceptDelay2_set(const char *param)
{
    uint32_t rxDelay;
    if (tiny_sscanf(param, "%lu", &rxDelay) != 1) {
        return AT_PARAM_ERROR;
    }
    else if (LmHandlerSetJoinRx2Delay(rxDelay) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("JN2DL : ");print_u(rxDelay);
    return AT_OK;

}



/* --------------- LoRaWAN join and send data commands --------------- */
ATError_t AT_Join(const char *param)
{

  if (LoRaMacIsBusy() == true || IsReJoinTimerStarted() == true) {
      return AT_BUSY_ERROR;
  }

  switch (FLASH_EFS_GetMemory()->bOtaa)
  {
    case 0:
      LmHandlerJoin(ACTIVATION_TYPE_ABP);
      break;

    default :
      LmHandlerJoin(ACTIVATION_TYPE_OTAA);
      break;

  }

  return AT_OK;
  /* USER CODE BEGIN AT_Join_2 */

  /* USER CODE END AT_Join_2 */
}




ATError_t AT_get_ajoin(const char *param)
{
    uint8_t onoff = FLASH_EFS_GetMemory()->bAutoJoin;
    if(onoff == 1) {
        AT_PRINTF("AJOIN : 1 \r\n");
    }
    else {
        AT_PRINTF("AJOIN : 0 \r\n");
    }

    return AT_OK;
}



ATError_t AT_set_ajoin(const char *param)
{
    uint8_t onoff;
    if (tiny_sscanf(param,"%hhu",&onoff)!= 1)
    {
        return AT_PARAM_ERROR;
    }

    if(onoff != 1) {
        onoff = 0;
    }

    FLASH_EFS_GetMemory()->bAutoJoin = onoff;
    FLASH_EFS_Write();

    AT_PRINTF("AJOIN : %d \r\n", onoff);
    return AT_OK;
}






ATError_t AT_Link_Check(const char *param)
{
    if (LmHandlerLinkCheckReq() != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    return AT_OK;
}




ATError_t AT_Send(const char *param)
{
    const char *buf = param;
    uint32_t appPort;
    
    int bufSize = strlen(param);
    
    LmHandlerMsgTypes_t isTxConfirmed;
    unsigned size = 0;

    UTIL_TIMER_Time_t nextTxIn = 0;
    LmHandlerErrorStatus_t lmhStatus = LORAMAC_HANDLER_ERROR;
    ATError_t status = AT_ERROR;

    /* read and set the application port */
    if (1 != tiny_sscanf(buf, "%u:", &appPort)) {
        return AT_PARAM_ERROR;
    }

    /* skip the application port */
    while (('0' <= buf[0]) && (buf[0] <= '9') && bufSize > 1) {
        buf ++;
        bufSize --;
    };

    if ((bufSize == 0) || (':' != buf[0])) {
        return AT_PARAM_ERROR;
    }
    else  {
        /* skip the char ':' */
        buf ++;
        bufSize --;
    }

    switch (buf[0]) {
        case '0':
            isTxConfirmed = LORAMAC_HANDLER_UNCONFIRMED_MSG;
            break;
        case '1':
            isTxConfirmed = LORAMAC_HANDLER_CONFIRMED_MSG;
            break;
        default:
            return AT_PARAM_ERROR;
    }

    if (bufSize > 0) {
        /* skip the acknowledge flag */
        buf ++;
        bufSize --;
    }

    if ((bufSize == 0) || (':' != buf[0])) {
        return AT_PARAM_ERROR;
    }
    else {
        /* skip the char ':' */
        buf ++;
        bufSize --;
    }

    while ((size < LORAWAN_APP_DATA_BUFFER_MAX_SIZE) && (bufSize > 0)) {
        AppData.Buffer[size] = buf[size];
        size++;
        bufSize--;
       
    }
    
    if (bufSize != 0) {
        return AT_PARAM_ERROR;
    }

    AppData.BufferSize = size;
    AppData.Port = appPort;

    lmhStatus = LmHandlerSend(&AppData, isTxConfirmed, &nextTxIn, false);

    switch (lmhStatus) {
        case LORAMAC_HANDLER_SUCCESS:
            status = (nextTxIn > 0) ? AT_DUTYCYCLE_RESTRICTED : AT_OK;
            break;
        case LORAMAC_HANDLER_BUSY_ERROR:
        case LORAMAC_HANDLER_COMPLIANCE_RUNNING:
            status = (LmHandlerJoinStatus() != LORAMAC_HANDLER_SET)?AT_NO_NET_JOINED:AT_BUSY_ERROR;
            break;
        case LORAMAC_HANDLER_NO_NETWORK_JOINED:
            status = AT_NO_NET_JOINED;
            break;
        case LORAMAC_HANDLER_DUTYCYCLE_RESTRICTED:
            status = AT_DUTYCYCLE_RESTRICTED;
            break;
        case LORAMAC_HANDLER_CRYPTO_ERROR:
            status = AT_CRYPTO_ERROR;
            break;
        case LORAMAC_HANDLER_ERROR:
        default:
            status = AT_ERROR;
            break;
    }

    return status;

}





ATError_t AT_SendB(const char *param)
{
    const char *buf = param;
    uint16_t bufSize = strlen(param);
    uint32_t appPort;
    
    LmHandlerMsgTypes_t isTxConfirmed;
    unsigned size = 0;
    char hex[3] = {0, 0, 0};
    UTIL_TIMER_Time_t nextTxIn = 0;
    LmHandlerErrorStatus_t lmhStatus = LORAMAC_HANDLER_ERROR;
    ATError_t status = AT_ERROR;

    /* read and set the application port */
    if (1 != tiny_sscanf(buf, "%u:", &appPort)) {
        AT_PRINTF("AT+SEND without the application port\r\n");
        return AT_PARAM_ERROR;
    }

    /* skip the application port */
    while (('0' <= buf[0]) && (buf[0] <= '9') && bufSize > 1) {
        buf ++;
        bufSize --;
    };

    if ((bufSize == 0) || (':' != buf[0])) {
        AT_PRINTF("AT+SEND missing : character after app port\r\n");
        return AT_PARAM_ERROR;
    }
    else  {
        /* skip the char ':' */
        buf ++;
        bufSize --;
    }

    switch (buf[0]) {
        case '0':
            isTxConfirmed = LORAMAC_HANDLER_UNCONFIRMED_MSG;
            break;
        case '1':
            isTxConfirmed = LORAMAC_HANDLER_CONFIRMED_MSG;
            break;
        default:
            AT_PRINTF("AT+SEND without the acknowledge flag\r\n");
            return AT_PARAM_ERROR;
    }

    if (bufSize > 0) {
        /* skip the acknowledge flag */
        buf ++;
        bufSize --;
    }

    if ((bufSize == 0) || (':' != buf[0])) {
        AT_PRINTF("AT+SEND missing : character after ack flag\r\n");
        return AT_PARAM_ERROR;
    }
    else {
        /* skip the char ':' */
        buf ++;
        bufSize --;
    }

    while ((size < LORAWAN_APP_DATA_BUFFER_MAX_SIZE) && (bufSize > 1)) {
        hex[0] = buf[size * 2];
        hex[1] = buf[size * 2 + 1];
        if (tiny_sscanf(hex, "%hhx", &AppData.Buffer[size]) != 1)  {
            return AT_PARAM_ERROR;
        }
        size++;
        bufSize -= 2;
    }
    
    if (bufSize != 0) {
        return AT_PARAM_ERROR;
    }

    AppData.BufferSize = size;
    AppData.Port = appPort;

    lmhStatus = LmHandlerSend(&AppData, isTxConfirmed, &nextTxIn, false);

    switch (lmhStatus) {
        case LORAMAC_HANDLER_SUCCESS:
            status = (nextTxIn > 0)?AT_DUTYCYCLE_RESTRICTED:AT_OK;
            break;
        case LORAMAC_HANDLER_BUSY_ERROR:
        case LORAMAC_HANDLER_COMPLIANCE_RUNNING:
            status = (LmHandlerJoinStatus() != LORAMAC_HANDLER_SET)?AT_NO_NET_JOINED:AT_BUSY_ERROR;
            break;
        case LORAMAC_HANDLER_NO_NETWORK_JOINED:
            status = AT_NO_NET_JOINED;
            break;
        case LORAMAC_HANDLER_DUTYCYCLE_RESTRICTED:
            status = AT_DUTYCYCLE_RESTRICTED;
            break;
        case LORAMAC_HANDLER_CRYPTO_ERROR:
            status = AT_CRYPTO_ERROR;
            break;
        case LORAMAC_HANDLER_ERROR:
        default:
            status = AT_ERROR;
            break;
    }

    return status;

}




ATError_t AT_SendH(const char *param)
{
    uint8_t     appPort;
    uint8_t     data_size = 0;

    ATError_t status = AT_ERROR;
    LmHandlerMsgTypes_t isTxConfirmed;
    LmHandlerErrorStatus_t lmhStatus;
    UTIL_TIMER_Time_t nextTxIn = 0;

    appPort = param[0];
    if(appPort == 0) {
        return AT_PARAM_ERROR;
    }
    
    isTxConfirmed = param[1];
    if(isTxConfirmed != LORAMAC_HANDLER_UNCONFIRMED_MSG && isTxConfirmed != LORAMAC_HANDLER_CONFIRMED_MSG) {
        return AT_PARAM_ERROR;
    }

    data_size = param[2];
    if(data_size >= 254) {
        return AT_PARAM_ERROR;
    }

    
    AppData.Port = appPort;
    AppData.BufferSize = data_size;
    memcpy(AppData.Buffer, &param[3], data_size);

    
    lmhStatus = LmHandlerSend(&AppData, isTxConfirmed, &nextTxIn, false);

    switch (lmhStatus) {
        case LORAMAC_HANDLER_SUCCESS:
            status = (nextTxIn > 0)?AT_DUTYCYCLE_RESTRICTED:AT_OK;
            break;
        case LORAMAC_HANDLER_BUSY_ERROR:
        case LORAMAC_HANDLER_COMPLIANCE_RUNNING:
            status = (LmHandlerJoinStatus() != LORAMAC_HANDLER_SET)?AT_NO_NET_JOINED:AT_BUSY_ERROR;
            break;
        case LORAMAC_HANDLER_NO_NETWORK_JOINED:
            status = AT_NO_NET_JOINED;
            break;
        case LORAMAC_HANDLER_DUTYCYCLE_RESTRICTED:
            status = AT_DUTYCYCLE_RESTRICTED;
            break;
        case LORAMAC_HANDLER_CRYPTO_ERROR:
            status = AT_CRYPTO_ERROR;
            break;
        case LORAMAC_HANDLER_ERROR:
        default:
            status = AT_ERROR;
            break;
    }

    return status;

}





ATError_t AT_get_receiveb(const char *param)
{
    uint8_t i;
    AT_PRINTF("RECVB : %d:", appLastRxData.Port);
    for (i = 0; i < appLastRxData.BufferSize; i++) {
        AT_PRINTF("%02x", appLastRxData.Buffer[i]);
    }
    AT_PRINTF("\r\n");

    appLastRxData.BufferSize = 0;
    return AT_OK;
}




ATError_t AT_get_receive(const char *param)
{
    AT_PRINTF("RECV : %d:", appLastRxData.Port);
    if(appLastRxData.BufferSize) {
        AT_PRINTF("%s", appLastRxData.Buffer);
    }
 
    AT_PRINTF("\r\n");
    appLastRxData.BufferSize = 0;
    
    return AT_OK;
}



ATError_t AT_set_receive(uint8_t Port, uint8_t BufferSize, uint8_t *Buffer, int8_t Snr, int8_t Rssi)
{
    appLastRxData.Port = Port;
    appLastRxData.BufferSize = BufferSize;
    
    memcpy(appLastRxData.Buffer, Buffer, BufferSize);
    appLastRxData.Buffer[BufferSize] = 0;

    nLastRxSnr = Snr;
    nLastRxRssi = Rssi;
    return AT_OK;
}





ATError_t AT_get_snr(const char *param)
{
    AT_PRINTF("SNR : %d\r\n", nLastRxSnr);
    return AT_OK;
}


ATError_t AT_get_rssi(const char *param)
{
    AT_PRINTF("RSSI : %d\r\n", nLastRxRssi);
    return AT_OK;
}


ATError_t AT_get_date(const char *param)
{
    // add-code :: get date AT_PRINTF("DATE : %s\r\n", "aaa");
    
    return AT_OK;
}



ATError_t AT_set_date(const char *param)
{
    uint32_t y;
    uint8_t m, d;

    if (tiny_sscanf(param, "%u:%hhu:%hhu", &y, &m, &d) != 3) {
        return AT_PARAM_ERROR;
    }

    if ((y > 99) || (m > 12) || (d > 31))
        return AT_PARAM_ERROR;


    // add-code : set date
    
    AT_PRINTF("DATE : %s\r\n", param);

    return AT_OK;
}



ATError_t AT_get_time(const char *param)
{

    // add-code :: get time  AT_PRINTF("TIME : %s\r\n", "aaa");

    return AT_OK;
}

ATError_t AT_set_time(const char *param)
{
    uint8_t h, m, s;
    if (tiny_sscanf(param, "%hhu:%hhu:%hhu", &h, &m, &s) != 3) {
        return AT_PARAM_ERROR;
    }

    if ((h > 23) || (m > 59) || (s > 59))
        return AT_PARAM_ERROR;

    // add-code :: set time 
    AT_PRINTF("TIME : %s\r\n", param);
    return AT_OK;
}



ATError_t AT_get_region(const char *param)
{
    #if defined(REGION_KR920)
        AT_PRINTF("REGION : %d\r\n", LORAMAC_REGION_KR920);
    #elif defined(REGION_US915)
        AT_PRINTF("REGION : %d\r\n", LORAMAC_REGION_US915);
    #elif defined(REGION_EU868)
        AT_PRINTF("REGION : %d\r\n", LORAMAC_REGION_EU868);
    #else
        return AT_PARAM_ERROR;
    #endif
    
    return AT_OK;
}


ATError_t AT_set_region(const char *param)
{
#if 0
    uint8_t region;

    if (tiny_sscanf(param, "%hhu", &region) != 1) {
        return AT_PARAM_ERROR;
    }
    
    AT_PRINTF("REGION : %d\r\n", region);
    return AT_OK;
#else
    /* Not support set region */
    return AT_PARAM_ERROR;
#endif
}



ATError_t AT_UplinkCounter_get(const char *param)
{
    AT_PRINTF("FCU : ");  print_u(LoRaMacCryptoGetFCnt());
    return AT_OK;
}


ATError_t AT_UplinkCounter_set(const char *param)
{
    uint32_t UpLinkCounter;

    if (tiny_sscanf(param, "%lu", &UpLinkCounter) != 1) {
        return AT_PARAM_ERROR;
    }
    LoRaMacCryptoSetFCnt(UpLinkCounter);
    AT_PRINTF("FCU : ");  print_u(UpLinkCounter);
    return AT_OK;
}


ATError_t AT_DownlinkCounter_get(const char *param)
{
    uint32_t DownLinkCounter;
    if(GetLastFcntDown(FCNT_DOWN, &DownLinkCounter) != LORAMAC_CRYPTO_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("FCD : ");  print_u(DownLinkCounter);
    return AT_OK;
}


ATError_t AT_DownlinkCounter_set(const char *param)
{
    uint32_t DownLinkCounter;
    if (tiny_sscanf(param, "%lu", &DownLinkCounter) != 1) {
        return AT_PARAM_ERROR;
    }
    
    UpdateFCntDown(FCNT_DOWN, DownLinkCounter);

    AT_PRINTF("FCD : ");  print_u(DownLinkCounter);
    return AT_OK;
}


ATError_t AT_link_check_req(const char *param)
{
    MlmeReq_t               mlmeReq;
    LmHandlerErrorStatus_t  lmhStatus;
    
    mlmeReq.Type = MLME_LINK_CHECK;
    LoRaMacMlmeRequest( &mlmeReq );
  
    AppData.BufferSize = 0;
    AppData.Port = 2;

    lmhStatus = LmHandlerSend(&AppData, LORAMAC_HANDLER_UNCONFIRMED_MSG, NULL, false);
    if(lmhStatus != LORAMAC_HANDLER_SUCCESS) {
        return AT_ERROR;
    }
    return AT_OK;
}


ATError_t AT_device_time_req(const char *param)
{
    MlmeReq_t               mlmeReq;
    LmHandlerErrorStatus_t  lmhStatus;
    
    mlmeReq.Type = MLME_DEVICE_TIME;

    LoRaMacMlmeRequest( &mlmeReq );

    AppData.BufferSize = 0;
    AppData.Port = 0;

    lmhStatus = LmHandlerSend(&AppData, LORAMAC_HANDLER_UNCONFIRMED_MSG, NULL, false);
    if(lmhStatus != LORAMAC_HANDLER_SUCCESS) {
        return AT_ERROR;
    }
    return AT_OK;
}





/* --------------- LoRaWAN network management commands --------------- */
ATError_t AT_version_get(const char *param)
{
    AT_PRINTF("VERSION : %s\r\n", LORA_APP_VERSION);
    return AT_OK;
}






ATError_t AT_NetworkID_get(const char *param)
{
    uint32_t networkId;
    if (LmHandlerGetNetworkID(&networkId) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }

    AT_PRINTF("NWKID : ");  print_d(networkId);
    return AT_OK;

}


ATError_t AT_NetworkID_set(const char *param)
{
  /* USER CODE BEGIN AT_NetworkID_set_1 */

  /* USER CODE END AT_NetworkID_set_1 */
  uint32_t networkId;
  if (tiny_sscanf(param, "%u", &networkId) != 1)
  {
    return AT_PARAM_ERROR;
  }

  if (networkId > 127)
  {
    return AT_PARAM_ERROR;
  }

  LmHandlerSetNetworkID(networkId);
  AT_PRINTF("NWKID : ");  print_d(networkId);
  
  return AT_OK;
}



ATError_t AT_DeviceClass_get(const char *param)
{
    DeviceClass_t currentClass;
    LoraInfo_t *loraInfo = LoraInfo_GetPtr();
    if (loraInfo == NULL) {
        return AT_ERROR;
    }

    if (LmHandlerGetCurrentClass(&currentClass) != LORAMAC_HANDLER_SUCCESS) {
        return AT_PARAM_ERROR;
    }
    else {
        AT_PRINTF("CLASS : %c\r\n", 'A' + currentClass);
    }

    return AT_OK;
}



ATError_t AT_DeviceClass_set(const char *param)
{
    LmHandlerErrorStatus_t errorStatus = LORAMAC_HANDLER_SUCCESS;
    LoraInfo_t *loraInfo = LoraInfo_GetPtr();
    
    if (loraInfo == NULL) {
        return AT_ERROR;
    }

    switch (param[0]) {
        case 'A':
            errorStatus = LmHandlerRequestClass(CLASS_A);
            if(errorStatus == LORAMAC_HANDLER_SUCCESS) {
                FLASH_EFS_GetMemory()->bClass = CLASS_A;
                FLASH_EFS_Write();
            }
            break;


        case 'C':
            errorStatus = LmHandlerRequestClass(CLASS_C);
            if(errorStatus == LORAMAC_HANDLER_SUCCESS) {
                FLASH_EFS_GetMemory()->bClass = CLASS_C;
                FLASH_EFS_Write();
            }
            break;
            
        default:
            return AT_PARAM_ERROR;
    }

    if (errorStatus == LORAMAC_HANDLER_NO_NETWORK_JOINED) {
        return AT_NO_NET_JOINED;
    }
    else if (errorStatus != LORAMAC_HANDLER_SUCCESS) {
        return AT_ERROR;
    }

    AT_PRINTF("CLASS : %c\r\n", param[0]);
    return AT_OK;
}





/* --------------- Radio tests commands --------------- */
ATError_t AT_test_txTone(const char *param)
{
    uint32_t freq;
    int32_t power;

    if(tiny_sscanf(param, "%u %hhd", &freq, &power) != 2) {
        return AT_BUSY_ERROR;
    }

    if (0U == TST_TxTone(freq, power)) {
        return AT_OK;
    }
    else {
        return AT_BUSY_ERROR;
    }

}

ATError_t AT_test_rxRssi(const char *param)
{
  /* USER CODE BEGIN AT_test_rxRssi_1 */

  /* USER CODE END AT_test_rxRssi_1 */
  if (0U == TST_RxRssi())
  {
    return AT_OK;
  }
  else
  {
    return AT_BUSY_ERROR;
  }
  /* USER CODE BEGIN AT_test_rxRssi_2 */

  /* USER CODE END AT_test_rxRssi_2 */
}

ATError_t AT_test_get_config(const char *param)
{
  /* USER CODE BEGIN AT_test_get_config_1 */

  /* USER CODE END AT_test_get_config_1 */
  testParameter_t testParam;
  uint32_t loraBW[7] = {7812, 15625, 31250, 62500, 125000, 250000, 500000};

  TST_get_config(&testParam);

  AT_PRINTF("1: Freq= %d Hz\r\n", testParam.freq);
  AT_PRINTF("2: Power= %d dBm\r\n", testParam.power);

  if (testParam.modulation == 0)
  {
    /*fsk*/
    AT_PRINTF("3: Bandwidth= %d Hz\r\n", testParam.bandwidth);
    AT_PRINTF("4: FSK datarate= %d bps\r\n", testParam.loraSf_datarate);
    AT_PRINTF("5: Coding Rate not applicable\r\n");
    AT_PRINTF("6: LNA State= %d  \r\n", testParam.lna);
    AT_PRINTF("7: PA Boost State= %d  \r\n", testParam.paBoost);
    AT_PRINTF("8: modulation FSK\r\n");
    AT_PRINTF("9: Payload len= %d Bytes\r\n", testParam.payloadLen);
    AT_PRINTF("10: FSK deviation= %d Hz\r\n", testParam.fskDev);
    AT_PRINTF("11: LowDRopt not applicable\r\n");
    AT_PRINTF("12: FSK gaussian BT product= %d \r\n", testParam.BTproduct);
  }
  else if (testParam.modulation == 1)
  {
    /*Lora*/
    AT_PRINTF("3: Bandwidth= %d (=%d Hz)\r\n", testParam.bandwidth, loraBW[testParam.bandwidth]);
    AT_PRINTF("4: SF= %d \r\n", testParam.loraSf_datarate);
    AT_PRINTF("5: CR= %d (=4/%d) \r\n", testParam.codingRate, testParam.codingRate + 4);
    AT_PRINTF("6: LNA State= %d  \r\n", testParam.lna);
    AT_PRINTF("7: PA Boost State= %d  \r\n", testParam.paBoost);
    AT_PRINTF("8: modulation LORA\r\n");
    AT_PRINTF("9: Payload len= %d Bytes\r\n", testParam.payloadLen);
    AT_PRINTF("10: Frequency deviation not applicable\r\n");
    AT_PRINTF("11: LowDRopt[0 to 2]= %d \r\n", testParam.lowDrOpt);
    AT_PRINTF("12 BT product not applicable\r\n");
  }
  else
  {
    AT_PRINTF("4: BPSK datarate= %d bps\r\n", testParam.loraSf_datarate);
  }

  AT_PRINTF("can be copy/paste in set cmd: AT+TCONF=%d:%d:%d:%d:4/%d:%d:%d:%d:%d:%d:%d:%d\r\n", testParam.freq,
            testParam.power,
            testParam.bandwidth, testParam.loraSf_datarate, testParam.codingRate + 4, \
            testParam.lna, testParam.paBoost, testParam.modulation, testParam.payloadLen, testParam.fskDev, testParam.lowDrOpt,
            testParam.BTproduct);
  return AT_OK;
  /* USER CODE BEGIN AT_test_get_config_2 */

  /* USER CODE END AT_test_get_config_2 */
}

ATError_t AT_test_set_config(const char *param)
{
  /* USER CODE BEGIN AT_test_set_config_1 */

  /* USER CODE END AT_test_set_config_1 */
  testParameter_t testParam = {0};
  uint32_t freq;
  int32_t power;
  uint32_t bandwidth;
  uint32_t loraSf_datarate;
  uint32_t codingRate;
  uint32_t lna;
  uint32_t paBoost;
  uint32_t modulation;
  uint32_t payloadLen;
  uint32_t fskDeviation;
  uint32_t lowDrOpt;
  uint32_t BTproduct;
  uint32_t crNum;

  if (13 == tiny_sscanf(param, "%d:%d:%d:%d:%d/%d:%d:%d:%d:%d:%d:%d:%d",
                        &freq,
                        &power,
                        &bandwidth,
                        &loraSf_datarate,
                        &crNum,
                        &codingRate,
                        &lna,
                        &paBoost,
                        &modulation,
                        &payloadLen,
                        &fskDeviation,
                        &lowDrOpt,
                        &BTproduct))
  {
    /*extend to new format for extended*/
  }
  else
  {
    return AT_PARAM_ERROR;
  }
  /*get current config*/
  TST_get_config(&testParam);

  /* 8: modulation check and set */
  /* first check because required for others */
  if (modulation == 0)
  {
    testParam.modulation = TEST_FSK;
  }
  else if (modulation == 1)
  {
    testParam.modulation = TEST_LORA;
  }
  else if (modulation == 2)
  {
    testParam.modulation = TEST_BPSK;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 1: frequency check and set */
  if (freq < 1000)
  {
    /*given in MHz*/
    testParam.freq = freq * 1000000;
  }
  else
  {
    testParam.freq = freq;
  }

  /* 2: power check and set */
  if ((power >= -9) && (power <= 22))
  {
    testParam.power = power;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 3: bandwidth check and set */
  if ((testParam.modulation == TEST_FSK) && (bandwidth >= 4800) && (bandwidth <= 467000))
  {
    testParam.bandwidth = bandwidth;
  }
  else if ((testParam.modulation == TEST_LORA) && (bandwidth <= BW_500kHz))
  {
    testParam.bandwidth = bandwidth;
  }
  else if (testParam.modulation == TEST_BPSK)
  {
    /* Not used */
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 4: datarate/spreading factor check and set */
  if ((testParam.modulation == TEST_FSK) && (loraSf_datarate >= 600) && (loraSf_datarate <= 300000))
  {
    testParam.loraSf_datarate = loraSf_datarate;
  }
  else if ((testParam.modulation == TEST_LORA) && (loraSf_datarate >= 5) && (loraSf_datarate <= 12))
  {
    testParam.loraSf_datarate = loraSf_datarate;
  }
  else if ((testParam.modulation == TEST_BPSK) && (loraSf_datarate <= 1000))
  {
    testParam.loraSf_datarate = loraSf_datarate;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 5: coding rate check and set */
  if ((testParam.modulation == TEST_FSK) || (testParam.modulation == TEST_BPSK))
  {
    /* Not used */
  }
  else if ((testParam.modulation == TEST_LORA) && ((codingRate >= 5) && (codingRate <= 8)))
  {
    testParam.codingRate = codingRate - 4;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 6: lna state check and set */
  if (lna <= 1)
  {
    testParam.lna = lna;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 7: pa boost check and set */
  if (paBoost <= 1)
  {
    /* Not used */
    testParam.paBoost = paBoost;
  }

  /* 9: payloadLen check and set */
  if ((payloadLen != 0) && (payloadLen < 256))
  {
    testParam.payloadLen = payloadLen;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 10: fsk Deviation check and set */
  if ((testParam.modulation == TEST_LORA) || (testParam.modulation == TEST_BPSK))
  {
    /* Not used */
  }
  else if ((testParam.modulation == TEST_FSK) && ((fskDeviation >= 600) && (fskDeviation <= 200000)))
  {
    /*given in MHz*/
    testParam.fskDev = fskDeviation;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 11: low datarate optimization check and set */
  if ((testParam.modulation == TEST_FSK) || (testParam.modulation == TEST_BPSK))
  {
    /* Not used */
  }
  else if ((testParam.modulation == TEST_LORA) && (lowDrOpt <= 2))
  {
    testParam.lowDrOpt = lowDrOpt;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  /* 12: FSK gaussian BT product check and set */
  if ((testParam.modulation == TEST_LORA) || (testParam.modulation == TEST_BPSK))
  {
    /* Not used */
  }
  else if ((testParam.modulation == TEST_FSK) && (BTproduct <= 4))
  {
    /*given in MHz*/
    testParam.BTproduct = BTproduct;
  }
  else
  {
    return AT_PARAM_ERROR;
  }

  TST_set_config(&testParam);

  return AT_OK;
  /* USER CODE BEGIN AT_test_set_config_2 */

  /* USER CODE END AT_test_set_config_2 */
}

ATError_t AT_test_tx(const char *param)
{
  /* USER CODE BEGIN AT_test_tx_1 */

  /* USER CODE END AT_test_tx_1 */
  const char *buf = param;
  uint32_t nb_packet;

  if (1 != tiny_sscanf(buf, "%u", &nb_packet))
  {
    AT_PRINTF("AT+TTX: nb packets sent is missing\r\n");
    return AT_PARAM_ERROR;
  }

  if (0U == TST_TX_Start(nb_packet))
  {
    return AT_OK;
  }
  else
  {
    return AT_BUSY_ERROR;
  }
  /* USER CODE BEGIN AT_test_tx_2 */

  /* USER CODE END AT_test_tx_2 */
}

ATError_t AT_test_rx(const char *param)
{
  /* USER CODE BEGIN AT_test_rx_1 */

  /* USER CODE END AT_test_rx_1 */
  const char *buf = param;
  uint32_t nb_packet;

  if (1 != tiny_sscanf(buf, "%u", &nb_packet))
  {
    AT_PRINTF("AT+TRX: nb expected packets is missing\r\n");
    return AT_PARAM_ERROR;
  }

  if (0U == TST_RX_Start(nb_packet))
  {
    return AT_OK;
  }
  else
  {
    return AT_BUSY_ERROR;
  }
  /* USER CODE BEGIN AT_test_rx_2 */

  /* USER CODE END AT_test_rx_2 */
}
ATError_t AT_Certif(const char *param)
{
  /* USER CODE BEGIN AT_Certif_1 */

  /* USER CODE END AT_Certif_1 */
  switch (param[0])
  {
    case '0':
      LmHandlerJoin(ACTIVATION_TYPE_ABP);
    case '1':
      LmHandlerJoin(ACTIVATION_TYPE_OTAA);
      break;
    default:
      return AT_PARAM_ERROR;
  }

  UTIL_TIMER_Create(&TxCertifTimer,  0xFFFFFFFFU, UTIL_TIMER_ONESHOT, OnCertifTimer, NULL);  /* 8s */
  UTIL_TIMER_SetPeriod(&TxCertifTimer,  8000);  /* 8s */
  UTIL_TIMER_Start(&TxCertifTimer);
  UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_LoRaCertifTx), UTIL_SEQ_RFU, CertifSend);

  return AT_OK;
  /* USER CODE BEGIN AT_Certif_2 */

  /* USER CODE END AT_Certif_2 */
}

ATError_t AT_test_tx_hopping(const char *param)
{
  /* USER CODE BEGIN AT_test_tx_hopping_1 */

  /* USER CODE END AT_test_tx_hopping_1 */
  const char *buf = param;
  uint32_t freq_start;
  uint32_t freq_stop;
  uint32_t delta_f;
  uint32_t nb_tx;

  testParameter_t test_param;
  uint32_t hop_freq;

  if (4 != tiny_sscanf(buf, "%u,%u,%u,%u", &freq_start, &freq_stop, &delta_f, &nb_tx))
  {
    return AT_PARAM_ERROR;
  }

  /*if freq is set in MHz, convert to Hz*/
  if (freq_start < 1000)
  {
    freq_start *= 1000000;
  }
  if (freq_stop < 1000)
  {
    freq_stop *= 1000000;
  }
  /**/
  hop_freq = freq_start;

  for (int i = 0; i < nb_tx; i++)
  {
    /*get current config*/
    TST_get_config(&test_param);

    /*increment frequency*/
    test_param.freq = hop_freq;
    /*Set new config*/
    TST_set_config(&test_param);

    PRINTF("Tx Hop at %dHz. %d of %d\r\n", hop_freq, i, nb_tx);

    if (0U != TST_TX_Start(1))
    {
      return AT_BUSY_ERROR;
    }

    hop_freq += delta_f;

    if (hop_freq > freq_stop)
    {
      hop_freq = freq_start;
    }
  }

  return AT_OK;

}



ATError_t AT_test_tx_LBT(const char *param)
{
	uint32_t freq; // Frequency
	int8_t power; // Tx power
	uint8_t sf; // Spreading Factor(datarate)
	uint8_t bw; // bandwidth
	uint8_t ds; // data size
	uint32_t period;

	if (tiny_sscanf(param, "%u %hhd %hhu %hhu %hhu %u", &freq, &power, &sf, &bw, &ds, &period) != 6)
	{
		return AT_PARAM_ERROR;
	}

	AT_PRINTF("freq(%u), power(%u dBm), sf(%u), BW(%u), len(%u), period(%u)\r\n",
	           freq, power, sf, bw, ds, period);

	TST_TX_Start_LBT(0xef, freq, power, sf, bw, ds, period*1000);
	return AT_OK;
    
}



ATError_t AT_test_rx_LBT(const char *param)
{
    uint32_t freq;
    uint8_t sf;
    uint8_t bw;
    
    if (tiny_sscanf(param, "%u %hhu %hhu", &freq, &sf, &bw) != 3)
    {
      return AT_PARAM_ERROR;
    }
    AT_PRINTF("RXTT: %u\r\n", freq);

	TST_RX_Start_LBT(freq, sf, bw);
    return AT_OK;
    
}


ATError_t AT_test_rxtx_LBT(const char *param)
{
    TST_RXTX_Start();
    return AT_OK;
}



ATError_t AT_test_stop(const char *param)
{
  /* USER CODE BEGIN AT_test_stop_1 */

  /* USER CODE END AT_test_stop_1 */
  TST_stop();
  AT_PRINTF("Test Stop\r\n");
  return AT_OK;
  /* USER CODE BEGIN AT_test_stop_2 */

  /* USER CODE END AT_test_stop_2 */
}




ATError_t AT_power_parm(const char *param) 
{

    if(param != NULL) {
    	if(param[0] >= '0' && param[0] <= '7') {
    		FLASH_EFS_GetMemory()->uPowerParm = param[0] - '0';
    	}
    }

    if(FLASH_EFS_GetMemory()->uPowerParm > 7) {
        FLASH_EFS_GetMemory()->uPowerParm = 7;
    }
    
    FLASH_EFS_Write();
    HAL_Delay(100);

    AT_PRINTF("PPARAM:%d\r\n", FLASH_EFS_GetMemory()->uPowerParm);
	return AT_OK;

}

ATError_t AT_write_register(const char *param)
{
  /* USER CODE BEGIN AT_write_register_1 */

  /* USER CODE END AT_write_register_1 */
  uint8_t add[2];
  uint16_t add16;
  uint8_t data;

  if (strlen(param) != 7)
  {
    return AT_PARAM_ERROR;
  }

  if (stringToData(param, add, 2) != 0)
  {
    return AT_PARAM_ERROR;
  }
  param += 5;
  if (stringToData(param, &data, 1) != 0)
  {
    return AT_PARAM_ERROR;
  }
  add16 = (((uint16_t)add[0]) << 8) + (uint16_t)add[1];
  Radio.Write(add16, data);

  return AT_OK;
  /* USER CODE BEGIN AT_write_register_2 */

  /* USER CODE END AT_write_register_2 */
}

ATError_t AT_read_register(const char *param)
{
  /* USER CODE BEGIN AT_read_register_1 */

  /* USER CODE END AT_read_register_1 */
  uint8_t add[2];
  uint16_t add16;
  uint8_t data;

  if (strlen(param) != 4)
  {
    return AT_PARAM_ERROR;
  }

  if (stringToData(param, add, 2) != 0)
  {
    return AT_PARAM_ERROR;
  }

  add16 = (((uint16_t)add[0]) << 8) + (uint16_t)add[1];
  data = Radio.Read(add16);
  AT_PRINTF("REG 0x%04X=0x%02X", add16, data);

  return AT_OK;
  /* USER CODE BEGIN AT_read_register_2 */

  /* USER CODE END AT_read_register_2 */
}



/* --------------- Information command --------------- */
ATError_t AT_bat_get(const char *param)
{
  return AT_OK;
}



ATError_t AT_set_factoryReset(const char *param)
{
    uint8_t       DevEui[8];
    sEfsItemType *pEfsData;

    pEfsData = FLASH_EFS_GetMemory();
    memcpy(DevEui, pEfsData->DevEui, 8);
    
    FLASH_EFS_FactoryReset();

    memcpy(pEfsData->DevEui, DevEui, 8);
    
    FLASH_EFS_Write();
    
    HAL_Delay(100);
    
    NVIC_SystemReset();
    
    return AT_OK;
  
}



ATError_t AT_set_bdload(const char *param)
{
    sEfsItemType *pEfsData;

    pEfsData = FLASH_EFS_GetMemory();
  
    pEfsData->dwResetReson = 0xFF05FF04;
    
    FLASH_EFS_Write();
    
    HAL_Delay(100);
    
    NVIC_SystemReset();
    
    return AT_OK;
  
}


ATError_t AT_PublicNetwork_get(const char *param)
{
    AT_PRINTF("PNM : %s\r\n", (FLASH_EFS_GetMemory()->bPublicNetwork == true) ? "Public" : "Private");
    return AT_OK;
}

ATError_t AT_PublicNetwork_set(const char *param)
{

    switch (param[0])
    {
        case '0':
        case '1':
            FLASH_EFS_GetMemory()->bPublicNetwork = param[0] - '0';
            FLASH_EFS_Write();
            AT_PRINTF("PNM : %s\r\n", (FLASH_EFS_GetMemory()->bPublicNetwork == 1) ? "Public" : "Private");
            return AT_OK;
            break;

        default:
            return AT_PARAM_ERROR;
    }
}



ATError_t AT_Sleep_set(const char *param)
{
    int nWakeUpSecond;
    
    if(param == NULL || param[0] == 0) {
        nWakeUpSecond = 5;
    }
    else {     
        nWakeUpSecond = atoi(param);
    }

    SetSleepStart(nWakeUpSecond);
    AT_PRINTF("SLEEP : %d\r\n", nWakeUpSecond);
    return AT_OK;
}




ATError_t AT_AINF_set(const char *param)
{
    AT_DevEUI_get(NULL);
    AT_AppEUI_get(NULL);
    AT_AppKey_get(NULL);
    AT_PublicNetwork_get(NULL);
    AT_NetworkJoinMode_get(NULL);
    AT_DeviceClass_get(NULL);
    AT_get_joinstatus(NULL);
    AT_NetworkID_get(NULL);
    AT_DevAddr_get(NULL);
    AT_NwkSKey_get(NULL);
    AT_AppSKey_get(NULL);
    return AT_OK;
}



ATError_t AT_LoRaState_get(const char *param)
{
    AT_PRINTF("LMSTATE : %d\r\n", GetLoRaMacState() );
    return AT_OK;
}




static void print_16_02x(uint8_t *pt)
{
  /* USER CODE BEGIN print_16_02x_1 */

  /* USER CODE END print_16_02x_1 */
  AT_PRINTF("%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X\r\n",
            pt[0], pt[1], pt[2], pt[3],
            pt[4], pt[5], pt[6], pt[7],
            pt[8], pt[9], pt[10], pt[11],
            pt[12], pt[13], pt[14], pt[15]);
  /* USER CODE BEGIN print_16_02x_2 */

  /* USER CODE END print_16_02x_2 */
}

static void print_8_02x(uint8_t *pt)
{
  /* USER CODE BEGIN print_8_02x_1 */

  /* USER CODE END print_8_02x_1 */
  AT_PRINTF("%02X%02X%02X%02X%02X%02X%02X%02X\r\n",
            pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6], pt[7]);
  /* USER CODE BEGIN print_8_02x_2 */

  /* USER CODE END print_8_02x_2 */
}

static void print_d(int32_t value)
{
  /* USER CODE BEGIN print_d_1 */

  /* USER CODE END print_d_1 */
  AT_PRINTF("%d\r\n", value);
  /* USER CODE BEGIN print_d_2 */

  /* USER CODE END print_d_2 */
}

static void print_u(uint32_t value)
{
  /* USER CODE BEGIN print_u_1 */

  /* USER CODE END print_u_1 */
  AT_PRINTF("%u\r\n", value);
  /* USER CODE BEGIN print_u_2 */

  /* USER CODE END print_u_2 */
}

static void OnCertifTimer(void *context)
{
  /* USER CODE BEGIN OnCertifTimer_1 */

  /* USER CODE END OnCertifTimer_1 */
  UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaCertifTx), CFG_SEQ_Prio_0);
  /* USER CODE BEGIN OnCertifTimer_2 */

  /* USER CODE END OnCertifTimer_2 */
}

static void CertifSend(void)
{
  /* USER CODE BEGIN CertifSend_1 */

  /* USER CODE END CertifSend_1 */
  AppData.Buffer[0] = 0x43;
  AppData.BufferSize = 1;
  AppData.Port = 99;

  /* Restart Tx to prevent a previous Join Failed */
  if (LmHandlerJoinStatus() != LORAMAC_HANDLER_SET)
  {
    UTIL_TIMER_Start(&TxCertifTimer);
  }
  LmHandlerSend(&AppData, LORAMAC_HANDLER_UNCONFIRMED_MSG, NULL, false);
}

static uint8_t Char2Nibble(char Char)
{
  if (((Char >= '0') && (Char <= '9')))
  {
    return Char - '0';
  }
  else if (((Char >= 'a') && (Char <= 'f')))
  {
    return Char - 'a' + 10;
  }
  else if ((Char >= 'A') && (Char <= 'F'))
  {
    return Char - 'A' + 10;
  }
  else
  {
    return 0xF0;
  }
  /* USER CODE BEGIN CertifSend_2 */

  /* USER CODE END CertifSend_2 */
}

static int32_t stringToData(const char *str, uint8_t *data, uint32_t Size)
{
  /* USER CODE BEGIN stringToData_1 */

  /* USER CODE END stringToData_1 */
  char hex[3];
  hex[2] = 0;
  int32_t ii = 0;
  while (Size-- > 0)
  {
    hex[0] = *str++;
    hex[1] = *str++;

    /*check if input is hex */
    if ((isHex(hex[0]) == -1) || (isHex(hex[1]) == -1))
    {
      return -1;
    }
    /*check if input is even nb of character*/
    if ((hex[1] == '\0') || (hex[1] == ','))
    {
      return -1;
    }
    data[ii] = (Char2Nibble(hex[0]) << 4) + Char2Nibble(hex[1]);

    ii++;
  }

  return 0;
  /* USER CODE BEGIN stringToData_2 */

  /* USER CODE END stringToData_2 */
}

static int32_t isHex(char Char)
{
  /* USER CODE BEGIN isHex_1 */

  /* USER CODE END isHex_1 */
  if (((Char >= '0') && (Char <= '9')) ||
      ((Char >= 'a') && (Char <= 'f')) ||
      ((Char >= 'A') && (Char <= 'F')))
  {
    return 0;
  }
  else
  {
    return -1;
  }
  /* USER CODE BEGIN isHex_2 */

  /* USER CODE END isHex_2 */
}

/* USER CODE BEGIN PrFD */

/* USER CODE END PrFD */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    usart_if.c
  * @author  MCD Application Team
  * @brief   Configuration of UART driver interface for hyperterminal communication
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "usart_if.h"



/* External variables ---------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_lpuart1_tx;
extern UART_HandleTypeDef hlpuart1;





/* Local variables ---------------------------------------------------------*/

uint8_t charRx;



const UTIL_ADV_TRACE_Driver_s UTIL_TraceDriver =
{
  vcom_Init,
  vcom_DeInit,
  vcom_ReceiveInit,
  vcom_Trace_DMA,
};



 static void (*TxCpltCallback)(void *);
 static void (*RxCpltCallback)(uint8_t *rxChar, uint16_t size, uint8_t error);






UTIL_ADV_TRACE_Status_t vcom_Init(void (*cb)(void *))
{
  TxCpltCallback = cb;
  MX_DMA_Init();
  MX_LPUART1_UART_Init();
  LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_28);
  return UTIL_ADV_TRACE_OK;
  
}


UTIL_ADV_TRACE_Status_t vcom_DeInit(void)
{
    /* ##-1- Reset peripherals ################################################## */
    __HAL_RCC_LPUART1_FORCE_RESET();
    __HAL_RCC_LPUART1_RELEASE_RESET();

    /* ##-2- MspDeInit ################################################## */
    HAL_UART_MspDeInit(&hlpuart1);

    /* ##-3- Disable the NVIC for DMA ########################################### */
    /* USER CODE BEGIN 1 */
    HAL_NVIC_DisableIRQ(DMA1_Channel5_IRQn);

    return UTIL_ADV_TRACE_OK;
  
}

void vcom_Trace(uint8_t *p_data, uint16_t size)
{
    HAL_UART_Transmit(&hlpuart1, p_data, size, 1000);
}

UTIL_ADV_TRACE_Status_t vcom_Trace_DMA(uint8_t *p_data, uint16_t size)
{
    HAL_UART_Transmit_DMA(&hlpuart1, p_data, size);
    return UTIL_ADV_TRACE_OK;
}




UTIL_ADV_TRACE_Status_t vcom_ReceiveInit(void (*RxCb)(uint8_t *rxChar, uint16_t size, uint8_t error))
{
    UART_WakeUpTypeDef WakeUpSelection;

    /*record call back*/
    RxCpltCallback = RxCb;

    /*Set wakeUp event on start bit*/
    WakeUpSelection.WakeUpEvent = UART_WAKEUP_ON_STARTBIT;

    HAL_UARTEx_StopModeWakeUpSourceConfig(&hlpuart1, WakeUpSelection);

    /* Make sure that no UART transfer is on-going */
    while (__HAL_UART_GET_FLAG(&hlpuart1, USART_ISR_BUSY) == SET);

    /* Make sure that UART is ready to receive)   */
    while (__HAL_UART_GET_FLAG(&hlpuart1, USART_ISR_REACK) == RESET);

    /* Enable USART interrupt */
    __HAL_UART_ENABLE_IT(&hlpuart1, UART_IT_WUF);

    /*Enable wakeup from stop mode*/
    HAL_UARTEx_EnableStopMode(&hlpuart1);

    /*Start LPUART receive on IT*/
    HAL_UART_Receive_IT(&hlpuart1, &charRx, 1);

    return UTIL_ADV_TRACE_OK;

}



void vcom_Resume(void)
{
    if (HAL_DMA_Init(&hdma_lpuart1_tx) != HAL_OK) {
        Error_Handler();
    }
}



void HAL_UART_TxCpltCallback(UART_HandleTypeDef *hlpuart1)
{
    TxCpltCallback(NULL);
}



void HAL_UART_RxCpltCallback(UART_HandleTypeDef *hlpuart1)
{
  if ((NULL != RxCpltCallback) && (HAL_UART_ERROR_NONE == hlpuart1->ErrorCode))
  {
    RxCpltCallback(&charRx, 1, 0);
  }
  HAL_UART_Receive_IT(hlpuart1, &charRx, 1);
}





#ifndef __FLASH_H__
#define __FLASH_H__



#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/





typedef enum {
  FLASHIF_OK = 0,
  FLASHIF_ERASEKO,
  FLASHIF_WRITINGCTRL_ERROR,
  FLASHIF_WRITING_ERROR,
  FLASHIF_PROTECTION_ERRROR
}sFlashErrType;


/* If the EFS Changed -------------------*/
#define EFS_INIT_DONE       0x58762539



typedef struct {
    uint32_t	dwResetReson;
    uint32_t    dwEfsInit;
    uint32_t    dwEfsSize;
    
    uint32_t    dwDevAddr;
    int32_t     nGmtTimcSync;

    uint8_t     bOtaa;
    uint8_t     bClass;
    uint8_t     bAutoJoin;
    uint8_t     bPublicNetwork;
    uint8_t     bChannelNbRep;  // unconfirmed
    uint8_t     bNbTrials;      // confirmed
    
    uint8_t     uDbgLevel;
    uint8_t     uTrimValA;
    uint8_t     uTrimValB;

    uint8_t     uPowerParm;

	uint8_t		DevEui[8];
	uint8_t		AppEui[8];
    uint8_t		AppKey[16];
	uint8_t		AppSKey[16];
	uint8_t		NwkSKey[16];
}sEfsItemType;

void			MX_FLASH_Init(void);

sEfsItemType	*FLASH_EFS_GetMemory(void);
void            FLASH_EFS_FactoryReset();
uint32_t		FLASH_EFS_Write(void);
uint32_t		FLASH_EFS_Read(void);


#ifdef __cplusplus
}
#endif

#endif /* __FLASH_H__ */



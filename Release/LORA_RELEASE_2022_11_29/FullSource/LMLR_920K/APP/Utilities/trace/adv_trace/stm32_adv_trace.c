/**
 ******************************************************************************
 * @file    stm32_adv_trace.c
 * @author  MCD Application Team
 * @brief   This file contains the advanced trace utility functions.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32_adv_trace.h"
#include "stdarg.h"
#include "stdio.h"

/** @addtogroup ADV_TRACE
 * @{
 */

/* Private defines -----------------------------------------------------------*/

/** @defgroup ADV_TRACE_Private_defines ADV_TRACE Privates defines
 *  @{
 */

/**
 *  @brief  memory address of the trace buffer location.
 *  This define can be used, to change the buffer location.
 *
 */
#if !defined(UTIL_ADV_TRACE_MEMLOCATION)
#define UTIL_ADV_TRACE_MEMLOCATION
#endif

#if defined(UTIL_ADV_TRACE_OVERRUN)
/**
 *  @brief  List the overrun status.
 *  list of the overrun status used to handle the overrun trace evacuation.
 *
 *  @note only valid if UTIL_ADV_TRACE_OVERRUN has been enabled inside utilities conf
 */
typedef enum {
  TRACE_OVERRUN_NONE = 0, /*!<overrun status none.                        */
  TRACE_OVERRUN_INDICATION, /*!<overrun status an indication shall be sent. */
  TRACE_OVERRUN_TRANSFERT, /*!<overrun status data transfer ongoing.       */
  TRACE_OVERRUN_EXECUTED, /*!<overrun status data transfer complete.      */
} TRACE_OVERRUN_STATUS;
#endif

#if defined(UTIL_ADV_TRACE_UNCHUNK_MODE)
/**
 *  @brief  List the unchunk status.
 *  list of the unchunk status used to handle the unchunk case.
 *
 *  @note only valid if UTIL_ADV_TRACE_UNCHUNK_MODE has been enabled inside utilities conf
 */
typedef enum {
  TRACE_UNCHUNK_NONE = 0,     /*!<unchunk status none.                            */
  TRACE_UNCHUNK_DETECTED,     /*!<unchunk status an unchunk has been detected.    */
  TRACE_UNCHUNK_TRANSFER      /*!<unchunk status an unchunk transfer is ongoing. */
} TRACE_UNCHUNK_STATUS;
#endif
/**
 * @}
 */

/**
 *  @brief  advanced macro to override to enable debug mode
 */
#ifndef UTIL_ADV_TRACE_DEBUG
#define UTIL_ADV_TRACE_DEBUG(...)
#endif
/* Private macros ------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/

/** @defgroup ADV_TRACE_private_typedef ADV_TRACE private typedef
 *  @{
 */

/**
 *  @brief  ADV_TRACE_Context.
 *  this structure contains all the data to handle the trace context.
 *
 *  @note some part of the context are depend with the selected switch inside the configuration file
 *  UTIL_ADV_TRACE_UNCHUNK_MODE, UTIL_ADV_TRACE_OVERRUN, UTIL_ADV_TRACE_CONDITIONNAL
 */

#define MAX_TRACE_CNT           20
#define MAX_TRACE_BUFF_SIZE     200

typedef struct {
    uint32_t    dbgLevel;

    uint16_t    read; /*!<read pointer the trace system.                              */
    uint16_t    write; /*!<write pointer the trace system.                            */
    uint16_t    lens[MAX_TRACE_CNT+1];
    uint16_t    sending;
} ADV_TRACE_Context;

/**
 *  @}
 */

/* Private variables ---------------------------------------------------------*/
/** @defgroup ADV_TRACE_private_variable ADV_TRACE private variable
 * private variable of the advanced trace system.
 *  @{
 */

/**
 * @brief trace context
 * this variable contains all the internal data of the advanced trace system.
 */
static ADV_TRACE_Context ADV_TRACE_Ctx;
static uint8_t ADV_TRACE_Buffer[MAX_TRACE_CNT+1][MAX_TRACE_BUFF_SIZE+1];


/**
 * @}
 */

/* Private function prototypes -----------------------------------------------*/
/** @defgroup ADV_TRACE_private_function ADV_TRACE private function
 *
 *  @{
 */
static void TRACE_TxCpltCallback(void *Ptr);
static UTIL_ADV_TRACE_Status_t TRACE_Send(void);


/**
 * @}
 */

/* Functions Definition ------------------------------------------------------*/

/** @addtogroup ADV_TRACE_exported_function
 *  @{
 */
UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_Init(void)
{
  /* initialize the Ptr for Read/Write */
  (void)UTIL_ADV_TRACE_MEMSET8(&ADV_TRACE_Ctx, 0x0, sizeof(ADV_TRACE_Context));
  (void)UTIL_ADV_TRACE_MEMSET8(&ADV_TRACE_Buffer, 0x0, sizeof(ADV_TRACE_Buffer));

  /* Allocate Lock resource */
  UTIL_ADV_TRACE_INIT_CRITICAL_SECTION();

  /* Initialize the Low Level interface */
  return UTIL_TraceDriver.Init(TRACE_TxCpltCallback);
}

UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_DeInit(void)
{
  /* Un-initialize the Low Level interface */
  return UTIL_TraceDriver.DeInit();
}

uint8_t UTIL_ADV_TRACE_IsBufferEmpty(void)
{
  /* check of the buffer is empty */
  if(ADV_TRACE_Ctx.read == ADV_TRACE_Ctx.write) return 1;
  return 0;
}



UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_StartRxProcess(void (*UserCallback)(uint8_t *PData, uint16_t Size, uint8_t Error))
{
  /* start the RX process */
  return UTIL_TraceDriver.StartRx(UserCallback);
}




UTIL_ADV_TRACE_Status_t UTIL_ADV_TRACE_FSend(uint32_t VerboseLevel, const char *strFormat, ...)
{
    va_list     vaArgs;
    uint16_t    buff_size = 0u;
    uint16_t    next_write;
    uint8_t		uSend = 0;
    
    next_write = (ADV_TRACE_Ctx.write + 1) % MAX_TRACE_CNT;
    if(next_write == ADV_TRACE_Ctx.read) {
        return UTIL_ADV_TRACE_GIVEUP;
    }

    /* check verbose level */
    if(ADV_TRACE_Ctx.dbgLevel < VerboseLevel) {
        return UTIL_ADV_TRACE_GIVEUP;
    }

    UTIL_ADV_TRACE_ENTER_CRITICAL_SECTION();

#if 1
    va_start( vaArgs, strFormat);
    buff_size = (uint16_t)UTIL_ADV_TRACE_VSNPRINTF((char *)&ADV_TRACE_Buffer[ADV_TRACE_Ctx.write][0], MAX_TRACE_BUFF_SIZE, strFormat, vaArgs);
    va_end(vaArgs);
#else
    buff_size = strlen(strFormat);
    strcpy((char*)&ADV_TRACE_Buffer[ADV_TRACE_Ctx.write][0], strFormat);
#endif
    
    if(buff_size > 0) {
        ADV_TRACE_Ctx.lens[ADV_TRACE_Ctx.write] = buff_size;
        ADV_TRACE_Buffer[ADV_TRACE_Ctx.write][buff_size] = 0;


        if(ADV_TRACE_Ctx.read == ADV_TRACE_Ctx.write && ADV_TRACE_Ctx.sending == 0){
    		uSend = 1;
    	}
        ADV_TRACE_Ctx.write = next_write;


        UTIL_ADV_TRACE_EXIT_CRITICAL_SECTION();

        if(uSend) {
        	return TRACE_Send();
        }
    }
    else {
        UTIL_ADV_TRACE_EXIT_CRITICAL_SECTION();
    }
    return UTIL_ADV_TRACE_OK;


}



static UTIL_ADV_TRACE_Status_t TRACE_Send(void)
{
    uint16_t                read;
    uint8_t*				ptr = NULL;
    UTIL_ADV_TRACE_Status_t ret = UTIL_ADV_TRACE_OK;
    
    
    UTIL_ADV_TRACE_ENTER_CRITICAL_SECTION();

    if(ADV_TRACE_Ctx.read != ADV_TRACE_Ctx.write && ADV_TRACE_Ctx.sending == 0) {
        
    	read = ADV_TRACE_Ctx.read;

    	ADV_TRACE_Ctx.sending = 1;
        ptr = &ADV_TRACE_Buffer[read][0];

        UTIL_ADV_TRACE_EXIT_CRITICAL_SECTION();

        UTIL_ADV_TRACE_PreSendHook();

        ret = UTIL_TraceDriver.Send(ptr, ADV_TRACE_Ctx.lens[read]);


    }
    else {
        UTIL_ADV_TRACE_EXIT_CRITICAL_SECTION();
    }

	return ret;
}

/**
 * @brief Tx callback called by the low layer level to inform a transfer complete
 * @param Ptr pointer not used only for HAL compatibility
 * @retval none
 */
static void TRACE_TxCpltCallback(void *Ptr)
{
	UTIL_ADV_TRACE_ENTER_CRITICAL_SECTION();

	ADV_TRACE_Ctx.sending = 0;
	if(ADV_TRACE_Ctx.read != ADV_TRACE_Ctx.write) {
		ADV_TRACE_Ctx.read = (ADV_TRACE_Ctx.read + 1) % MAX_TRACE_CNT;
		// ADV_TRACE_Ctx.read = ADV_TRACE_Ctx.write;
	}

	UTIL_ADV_TRACE_PostSendHook();
	UTIL_ADV_TRACE_EXIT_CRITICAL_SECTION();

	TRACE_Send();
}





void UTIL_ADV_TRACE_SetVerboseLevel(uint8_t Level)
{
  ADV_TRACE_Ctx.dbgLevel = Level;
}

uint8_t UTIL_ADV_TRACE_GetVerboseLevel(void)
{
  return ADV_TRACE_Ctx.dbgLevel;
}


__WEAK void UTIL_ADV_TRACE_PreSendHook(void)
{
}

__WEAK void UTIL_ADV_TRACE_PostSendHook(void)
{
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

